DOCKER_COMPOSE=docker-compose
EXEC=$(DOCKER_COMPOSE) exec app entrypoint
COMPOSER=$(EXEC) composer
CONSOLE=./bin/console
PHPUNIT=./vendor/bin/phpunit

.DEFAULT_GOAL := help
.PHONY: build install reset start stop clear clean
.PHONY: db-reset db-drop db-create db-reset fixtures
.PHONY: bash
.PHONY: csfix test qa behat
.PHONY: help

help:
	@grep -E '(^[a-zA-Z_-]+:.*?##.*$$)|(^##)' $(MAKEFILE_LIST) | awk 'BEGIN {FS = ":.*?## "}; {printf "\033[32m%-30s\033[0m %s\n", $$1, $$2}' | sed -e 's/\[32m##/[33m/'

##
## Project setup
##---------------------------------------------------------------------------

install: ## Install and start the project
install: .env build start vendor db-migrate fixtures

reset: ## Stop and start a fresh install of the project
reset: clean kill install

build:
	$(DOCKER_COMPOSE) build --pull

kill:
	$(DOCKER_COMPOSE) kill
	$(DOCKER_COMPOSE) down --volumes --remove-orphans

start: ## Start the project
	$(DOCKER_COMPOSE) up -d --remove-orphans --no-recreate

stop: ## Stop the project
	$(DOCKER_COMPOSE) stop

clear: ## Remove all the cache, the logs, the sessions and the built assets
	$(EXEC) rm -rf var/cache/*
	$(EXEC) rm -rf var/sessions/*
	$(EXEC) rm -rf var/log/*
	@if [ -f var/.php_cs.cache ]; \
	then\
		$(EXEC) rm var/.php_cs.cache ; \
	fi

clean: ## Stop the Project and Remove generated files
clean: clear
	$(EXEC) rm -rf vendor

composer.lock: composer.json
	$(COMPOSER) update --lock --no-scripts --no-interaction

vendor: composer.lock
	$(COMPOSER) install
	$(COMPOSER) dump-autoload

.env: .env.dist
	@if [ -f .env ]; \
	then\
		echo '\033[1;41m/!\ The .env.dist file has changed. Please check your .env file (this message will not be displayed again).\033[0m';\
		touch .env;\
		exit 1;\
	else\
		echo cp .env.dist .env;\
		cp .env.dist .env;\
	fi

##
## DataBase as db
##---------------------------------------------------------------------------
db-reset: db-drop db-create db-migrate fixtures

db-migrate:
	sleep 10
	$(EXEC) $(CONSOLE) doctrine:migrations:migrate --no-interaction
db-diff:
	$(EXEC) $(CONSOLE) doctrine:migrations:diff --no-interaction
db-drop:
	$(EXEC) $(CONSOLE) doctrine:database:drop --force
db-create:
	$(EXEC) $(CONSOLE) doctrine:database:create
fixtures:
	$(EXEC) $(CONSOLE) hautelook:fixtures:load --purge-with-truncate --no-interaction
##
## Tools
##---------------------------------------------------------------------------
bash: ## go to docker app bash
	$(EXEC) bash

fix-entity:
	$(EXEC) $(CONSOLE) make:entity --regenerate App

destroy: ## kill/delete images && container docker
	docker stop $(docker ps -a -q)
	docker rm $(docker ps -a -q)
	docker rmi $(docker images -q) --force

docker-reboot: ## restart docker on ubuntu
	/etc/init.d/docker restart

##
## QA
##---------------------------------------------------------------------------
qa: ## launch cs-fixer && phpunit
qa: clear csfix test behat

test:  ## launch phpunit
	$(EXEC) $(PHPUNIT)
test-c:  ## launch phpunit with coverage
	$(EXEC) $(PHPUNIT) --coverage-html ./var/html

cs: ## php-cs-fixer (http://cs.sensiolabs.org)
	php-cs-fixer fix --diff --dry-run --no-interaction -v

csfix: ## php-cs-fixer (http://cs.sensiolabs.org)
	php-cs-fixer fix --diff --no-interaction -v

behat:
	$(EXEC) vendor/bin/behat

