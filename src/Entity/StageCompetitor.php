<?php

namespace App\Entity;

use App\Entity\Interfaces\EntityIdentityInterface;
use App\Entity\Traits\EntityCompetitorTrait;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use App\Entity\Traits\EntityIdentityTrait;
use App\Entity\Traits\TimestampableTrait;

/**
 * StageCompetitor
 *
 * @ORM\Table(
 *     name="stage_competitor",
 *     indexes={
 *         @ORM\Index(name="fk_stage_competitor_stage1_idx", columns={"stage_id"}),
 *         @ORM\Index(name="fk_stage_competitor_competitor1_idx", columns={"competitor_id"}),
 *         @ORM\Index(name="fk_stage_competitor_country1_idx", columns={"country"}),
 *         @ORM\Index(name="fk_stage_competitor_club1_idx", columns={"club_id"})
 *     },
 *     uniqueConstraints={
 *         @ORM\UniqueConstraint(
 *             name="competitor_has_joined_stage",
 *             columns={"competitor_id", "stage_id"}
 *         ),
 *         @ORM\UniqueConstraint(
 *             name="uuid_UNIQUE",
 *             columns={"uuid"}
 *         )
 *     }
 * )
 * @ORM\Entity(repositoryClass="App\Repository\StageCompetitorRepository")
 * @ORM\HasLifecycleCallbacks
 */
class StageCompetitor implements EntityIdentityInterface
{
    use EntityIdentityTrait {
        EntityIdentityTrait::__construct as private __eitConstruct;
    }
    use TimestampableTrait;
    use EntityCompetitorTrait;

    /**
     * @ORM\ManyToOne(targetEntity="ChampionshipCompetitor", inversedBy="stageCompetitors")
     * @ORM\JoinColumns({
     *     @ORM\JoinColumn(name="championship_competitor_id", referencedColumnName="id", nullable=false)
     * })
     */
    private $championshipCompetitor;

    /**
     * @ORM\OneToMany(targetEntity="LoungeCompetitor", mappedBy="stageCompetitor")
     */
    private $loungeCompetitors;

    /**
     * @var Stage
     *
     * @ORM\ManyToOne(targetEntity="Stage", inversedBy="stageCompetitors")
     * @ORM\JoinColumns({
     *     @ORM\JoinColumn(name="stage_id", referencedColumnName="id")
     * })
     */
    private $stage;

    public function __construct()
    {
        $this->__eitConstruct();
        $this->loungeCompetitors = new ArrayCollection();
    }

    public function getStage(): ?Stage
    {
        return $this->stage;
    }

    public function setStage(?Stage $stage): self
    {
        $this->stage = $stage;

        return $this;
    }

    public function getChampionshipCompetitor(): ?ChampionshipCompetitor
    {
        return $this->championshipCompetitor;
    }

    public function setChampionshipCompetitor(?ChampionshipCompetitor $championshipCompetitor): self
    {
        $this->championshipCompetitor = $championshipCompetitor;
        if (null !== $this->championshipCompetitor) {
            $this->setCompetitor($this->championshipCompetitor->getCompetitor());
        }

        return $this;
    }

    /**
     * @return Collection|LoungeCompetitor[]
     */
    public function getLoungeCompetitors(): Collection
    {
        return $this->loungeCompetitors;
    }

    public function addLoungeCompetitor(LoungeCompetitor $loungeCompetitor): self
    {
        if (!$this->loungeCompetitors->contains($loungeCompetitor)) {
            $this->loungeCompetitors[] = $loungeCompetitor;
            $loungeCompetitor->setStageCompetitor($this);
        }

        return $this;
    }

    public function removeLoungeCompetitor(LoungeCompetitor $loungeCompetitor): self
    {
        if ($this->loungeCompetitors->contains($loungeCompetitor)) {
            $this->loungeCompetitors->removeElement($loungeCompetitor);
            if ($loungeCompetitor->getStageCompetitor() === $this) {
                $loungeCompetitor->setStageCompetitor(null);
            }
        }

        return $this;
    }
}
