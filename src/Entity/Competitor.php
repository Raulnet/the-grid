<?php

namespace App\Entity;

use App\Entity\Interfaces\EntityIdentityInterface;
use Doctrine\ORM\Mapping as ORM;
use App\Entity\Traits\EntityIdentityTrait;
use App\Entity\Traits\TimestampableTrait;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * Competitor
 *
 * @ORM\Table(name="competitor",
 *     uniqueConstraints={
 *         @ORM\UniqueConstraint(
 *             name="uuid_UNIQUE",
 *             columns={"uuid"}
 *         )
 *     },
 *     indexes={@ORM\Index(name="fk_competitor_club1_idx", columns={"club_id"})}
 * )
 * @ORM\Entity(repositoryClass="App\Repository\CompetitorRepository")
 * @ORM\HasLifecycleCallbacks
 */
class Competitor implements EntityIdentityInterface
{
    use EntityIdentityTrait;
    use TimestampableTrait;

    /**
     * @var string
     *
     * @ORM\Column(type="string", length=140, nullable=false)
     * @Assert\NotBlank(message="constraint.exception.username.could.not.be.empty")
     */
    private $username;

    /**
     * @var string
     *
     * @ORM\Column(type="string", length=3, nullable=false)
     * @Assert\Country(message="constraint.exception.need.to.be.country.code")
     */
    private $country;

    /**
     * @var string
     *
     * @ORM\Column(name="bid", type="string", length=12, nullable=false)
     * @Assert\NotBlank(message="constraint.exception.bid.could.not.be.empty")
     */
    private $bid;

    /**
     * @var Club
     *
     * @ORM\ManyToOne(targetEntity="Club", inversedBy="competitors")
     * @ORM\JoinColumns({
     *     @ORM\JoinColumn(name="club_id", referencedColumnName="id")
     * })
     */
    private $club;

    public function getUsername(): ?string
    {
        return $this->username;
    }

    public function setUsername(string $username): self
    {
        $this->username = $username;

        return $this;
    }

    public function getCountry(): ?string
    {
        return $this->country;
    }

    public function setCountry(string $country): self
    {
        $this->country = $country;

        return $this;
    }

    public function getBid(): ?string
    {
        return $this->bid;
    }

    public function setBid(string $bid): self
    {
        $this->bid = $bid;

        return $this;
    }

    public function getClub(): ?Club
    {
        return $this->club;
    }

    public function setClub(?Club $club): self
    {
        $this->club = $club;

        return $this;
    }
}
