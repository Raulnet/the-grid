<?php

namespace App\Entity;

use App\Entity\Interfaces\EntityIdentityInterface;
use App\Entity\Traits\EntityCompetitorTrait;
use Doctrine\ORM\Mapping as ORM;
use App\Entity\Traits\EntityIdentityTrait;
use App\Entity\Traits\TimestampableTrait;

/**
 * Performance
 *
 * @ORM\Table(
 *     name="performance",
 *     indexes={
 *         @ORM\Index(name="fk_performance_lounge_competitor1_idx", columns={"lounge_competitor_id"}),
 *         @ORM\Index(name="fk_performance_country1_idx", columns={"country"}),
 *         @ORM\Index(name="fk_performance_round1_idx", columns={"round_id"})
 *     },
 *     uniqueConstraints={
 *         @ORM\UniqueConstraint(
 *             name="competitor_has_joined_championship",
 *             columns={"competitor_id", "round_id"}
 *         ),
 *         @ORM\UniqueConstraint(
 *             name="uuid_UNIQUE",
 *             columns={"uuid"}
 *         )
 *     }
 * )
 * @ORM\Entity(repositoryClass="App\Repository\PerformanceRepository")
 * @ORM\HasLifecycleCallbacks
 */
class Performance implements EntityIdentityInterface
{
    use EntityIdentityTrait;
    use TimestampableTrait;
    use EntityCompetitorTrait;

    /**
     * @var int|null
     *
     * @ORM\Column(name="performance", type="integer", nullable=true)
     */
    private $performance;

    /**
     * @var LoungeCompetitor
     *
     * @ORM\ManyToOne(targetEntity="LoungeCompetitor")
     * @ORM\JoinColumns({
     *     @ORM\JoinColumn(name="lounge_competitor_id", referencedColumnName="id")
     * })
     */
    private $loungeCompetitor;

    /**
     * @var Round
     *
     * @ORM\ManyToOne(targetEntity="Round")
     * @ORM\JoinColumns({
     *     @ORM\JoinColumn(name="round_id", referencedColumnName="id")
     * })
     */
    private $round;

    public function getPerformance(): ?int
    {
        return $this->performance;
    }

    public function setPerformance(?int $performance): self
    {
        $this->performance = $performance;

        return $this;
    }

    public function getLoungeCompetitor(): ?LoungeCompetitor
    {
        return $this->loungeCompetitor;
    }

    public function setLoungeCompetitor(?LoungeCompetitor $loungeCompetitor): self
    {
        $this->loungeCompetitor = $loungeCompetitor;
        if ($loungeCompetitor instanceof LoungeCompetitor) {
            $this->setCompetitor($loungeCompetitor->getCompetitor());
        }

        return $this;
    }

    public function getRound(): ?Round
    {
        return $this->round;
    }

    public function setRound(?Round $round): self
    {
        $this->round = $round;

        return $this;
    }
}
