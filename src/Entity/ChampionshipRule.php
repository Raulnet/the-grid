<?php

namespace App\Entity;

use App\Entity\Interfaces\EntityContentInterface;
use App\Entity\Interfaces\EntityIdentityInterface;
use App\Entity\Traits\EntityContentTrait;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use App\Entity\Traits\EntityIdentityTrait;
use App\Entity\Traits\TimestampableTrait;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * ChampionshipRule
 *
 * @ORM\Table(name="championship_rule",
 *     uniqueConstraints={
 *         @ORM\UniqueConstraint(
 *             name="uuid_UNIQUE",
 *             columns={"uuid"}
 *         )
 *     }
 * )
 * @ORM\Entity(repositoryClass="App\Repository\ChampionshipRuleRepository")
 * @ORM\HasLifecycleCallbacks
 */
class ChampionshipRule implements EntityIdentityInterface, EntityContentInterface
{
    use EntityIdentityTrait {
        EntityIdentityTrait::__construct as private __eitConstruct;
    }
    use EntityContentTrait;
    use TimestampableTrait;

    /**
     * @var string
     *
     * @ORM\Column(type="text", nullable=false)
     * @Assert\NotBlank(message="constraint.exception.rules.could.not.be.empty")
     */
    private $rules;

    /**
     * @ORM\OneToMany(targetEntity="Championship", mappedBy="championshipRule")
     */
    private $championships;

    public function __construct()
    {
        $this->__eitConstruct();
        $this->championships = new ArrayCollection();
    }

    public function getRules(): ?string
    {
        return $this->rules;
    }

    public function setRules(string $rules): self
    {
        $this->rules = $rules;

        return $this;
    }

    /**
     * @return Collection|Championship[]
     */
    public function getChampionships(): Collection
    {
        return $this->championships;
    }

    public function addChampionship(Championship $championship): self
    {
        if (!$this->championships->contains($championship)) {
            $this->championships[] = $championship;
            $championship->setChampionshipRule($this);
        }

        return $this;
    }

    public function removeChampionship(Championship $championship): self
    {
        if ($this->championships->contains($championship)) {
            $this->championships->removeElement($championship);
            if ($championship->getChampionshipRule() === $this) {
                $championship->setChampionshipRule(null);
            }
        }

        return $this;
    }
}
