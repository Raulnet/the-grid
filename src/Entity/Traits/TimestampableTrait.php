<?php
/**
 * Created by PhpStorm.
 * User: raulnet
 * Date: 09/12/18
 * Time: 19:56
 */

namespace App\Entity\Traits;

use Doctrine\ORM\Mapping as ORM;

trait TimestampableTrait
{
    /**
     * @var \DateTime
     *
     * @ORM\Column(name="date_creation", type="datetime", nullable=false)
     */
    private $dateCreation;
    /**
     * @var \DateTime
     *
     * @ORM\Column(name="date_update", type="datetime", nullable=false)
     */
    private $dateUpdate;
    /**
     * @ORM\Column(name="date_deleted", type="datetime", nullable=true)
     */
    private $dateDeleted;

    /**
     * Auto-update createdAt and updatedAt automatically.
     *
     * @ORM\PrePersist
     * @ORM\PreUpdate
     */
    public function autoUpdate(): void
    {
        if (null === $this->dateCreation) {
            $this->dateCreation = new \DateTime('now');
        }
        $this->dateUpdate = new \DateTime('now');
    }

    public function getDateCreation(): ?\DateTime
    {
        return $this->dateCreation;
    }

    public function setDateCreation(\DateTime $dateCreation): self
    {
        $this->dateCreation = $dateCreation;

        return $this;
    }

    public function getDateUpdate(): ?\DateTime
    {
        return $this->dateUpdate;
    }

    public function setDateUpdate(\DateTime $dateUpdate): self
    {
        $this->dateUpdate = $dateUpdate;

        return $this;
    }

    public function getDateDeleted(): ?\DateTime
    {
        return $this->dateDeleted;
    }

    public function setDateDeleted(?\DateTime $dateTime): self
    {
        $this->dateDeleted = $dateTime;

        return $this;
    }

    public function isDeleted(): bool
    {
        return (bool) $this->getDateDeleted();
    }
}
