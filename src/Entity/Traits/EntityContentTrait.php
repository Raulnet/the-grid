<?php
/**
 * Created by PhpStorm.
 * User: raulnet
 * Date: 28/12/18
 * Time: 21:22
 */

namespace App\Entity\Traits;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

trait EntityContentTrait
{
    /**
     * @ORM\Column(name="title", type="string", length=80, nullable=false)
     * @Assert\NotBlank(message="constraint.exception.title.could.not.be.empty")
     * @Assert\Length(max="80", maxMessage="constraint.exception.title.cannot.be.longer")
     */
    private $title;

    /**
     * @ORM\Column(name="description", type="text", nullable=false)
     * @Assert\NotBlank(message="constraint.exception.description.could.not.be.empty")
     */
    private $description;

    public function getTitle(): ?string
    {
        return $this->title;
    }

    public function setTitle(string $title): self
    {
        $this->title = $title;

        return $this;
    }

    public function getDescription(): ?string
    {
        return $this->description;
    }

    public function setDescription(string $description): self
    {
        $this->description = $description;

        return $this;
    }

    public function __toString(): string
    {
        return $this->title ?: '';
    }
}
