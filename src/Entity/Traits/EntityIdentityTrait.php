<?php
/**
 * Created by PhpStorm.
 * User: raulnet
 * Date: 28/12/18
 * Time: 20:18
 */

namespace App\Entity\Traits;

use Doctrine\ORM\Mapping as ORM;
use Ramsey\Uuid\Uuid;
use Symfony\Component\Validator\Constraints as Assert;
use Swagger\Annotations as SWG;

trait EntityIdentityTrait
{
    /**
     * @ORM\Column(type="integer", options={"unsigned": true})
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     * @SWG\Property(description="The unique identifier of the entity.")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(type="uuid")
     * @Assert\Uuid(message="constraint.exception.uuid.pattern.could.be.respected")
     * @SWG\Property(description="The unique uuid identifier of the entity.")
     */
    private $uuid;

    public function __construct()
    {
        $this->uuid = Uuid::uuid4();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getUuid(): string
    {
        return $this->uuid;
    }

    public function setUuid(string $uuid): void
    {
        $this->uuid = $uuid;
    }
}
