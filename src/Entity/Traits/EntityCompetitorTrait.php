<?php
/**
 * Created by PhpStorm.
 * User: raulnet
 * Date: 30/12/18
 * Time: 15:24
 */

namespace App\Entity\Traits;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
use App\Entity\Club;
use App\Entity\Competitor;

trait EntityCompetitorTrait
{
    /**
     * @var int|null
     *
     * @ORM\Column(type="integer", nullable=true)
     */
    private $score;

    /**
     * @var string|null
     *
     * @ORM\Column(type="string", length=3, nullable=true)
     * @Assert\Country(message="constraint.exception.need.to.be.country.code")
     */
    private $country;

    /**
     * @var Club
     *
     * @ORM\ManyToOne(targetEntity="Club")
     * @ORM\JoinColumns({
     *     @ORM\JoinColumn(name="club_id", referencedColumnName="id")
     * })
     */
    private $club;

    /**
     * @var Competitor
     *
     * @ORM\ManyToOne(targetEntity="Competitor")
     * @ORM\JoinColumns({
     *     @ORM\JoinColumn(name="competitor_id", referencedColumnName="id")
     * })
     */
    private $competitor;

    public function getScore(): ?int
    {
        return $this->score;
    }

    public function setScore(?int $score): self
    {
        $this->score = $score;

        return $this;
    }

    public function getCountry(): ?string
    {
        return $this->country;
    }

    public function setCountry(?string $country): self
    {
        $this->country = $country;

        return $this;
    }

    public function getClub(): ?Club
    {
        return $this->club;
    }

    public function setClub(?Club $club): self
    {
        $this->club = $club;

        return $this;
    }

    public function getCompetitor(): ?Competitor
    {
        return $this->competitor;
    }

    public function setCompetitor(?Competitor $competitor): self
    {
        $this->competitor = $competitor;
        if (null !== $competitor) {
            $this->setClub($competitor->getClub());
            $this->setCountry($competitor->getCountry());
        }

        return $this;
    }
}
