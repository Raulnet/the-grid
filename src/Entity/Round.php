<?php

namespace App\Entity;

use App\Entity\Enum\RoundEnum;
use App\Entity\Interfaces\EntityContentInterface;
use App\Entity\Interfaces\EntityIdentityInterface;
use App\Entity\Interfaces\EntitySequenceInterface;
use App\Entity\Traits\EntityContentTrait;
use App\Entity\Traits\EntitySequenceTrait;
use Doctrine\ORM\Mapping as ORM;
use App\Entity\Traits\EntityIdentityTrait;
use App\Entity\Traits\TimestampableTrait;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * Round
 *
 * @ORM\Table(name="round",
 *     uniqueConstraints={
 *         @ORM\UniqueConstraint(
 *             name="uuid_UNIQUE",
 *             columns={"uuid"}
 *         ),
 *         @ORM\UniqueConstraint(
 *             name="round_sequence_UNIQUE",
 *             columns={"sequence", "stage_id"}
 *         )
 *     },
 *     indexes={@ORM\Index(name="fk_round_stage1_idx", columns={"stage_id"})}
 * )
 * @ORM\Entity(repositoryClass="App\Repository\RoundRepository")
 * @ORM\HasLifecycleCallbacks
 */
class Round implements EntityIdentityInterface, EntityContentInterface, EntitySequenceInterface
{
    use EntityIdentityTrait {
        EntityIdentityTrait::__construct as private __eitConstruct;
    }
    use EntityContentTrait;
    use TimestampableTrait;
    use EntitySequenceTrait;

    /**
     * @ORM\ManyToOne(targetEntity="Stage", inversedBy="rounds")
     * @ORM\JoinColumns({
     *     @ORM\JoinColumn(name="stage_id", referencedColumnName="id")
     * })
     */
    private $stage;

    /**
     * @ORM\Column(length=10, nullable=false)
     * @Assert\Choice("App\Entity\Enum\RoundEnum::listPlaces")
     */
    private $currentPlace;

    public function __construct()
    {
        $this->__eitConstruct();
        $this->timeZone = 'UTC';
        $this->currentPlace = RoundEnum::PLACE_BUILD;
    }

    public function getStage(): ?Stage
    {
        return $this->stage;
    }

    public function setStage(?Stage $stage): self
    {
        $this->stage = $stage;

        return $this;
    }

    public function getCurrentPlace(): ?string
    {
        return $this->currentPlace;
    }

    public function setCurrentPlace(string $currentPlace): self
    {
        $this->currentPlace = $currentPlace;

        return $this;
    }
}
