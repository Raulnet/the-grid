<?php

namespace App\Entity;

use App\Entity\Interfaces\EntityContentInterface;
use App\Entity\Interfaces\EntityIdentityInterface;
use App\Entity\Traits\EntityContentTrait;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use App\Entity\Traits\EntityIdentityTrait;
use App\Entity\Traits\TimestampableTrait;

/**
 * Championship
 *
 * @ORM\Table(
 *     name="championship",
 *     indexes={
 *         @ORM\Index(name="fk_championship_championship_rule1_idx", columns={"championship_rule_id"})
 *     },
 *     uniqueConstraints={
 *         @ORM\UniqueConstraint(
 *             name="uuid_UNIQUE",
 *             columns={"uuid"}
 *         )
 *     }
 * )
 * @ORM\Entity(repositoryClass="App\Repository\ChampionshipRepository")
 * @ORM\HasLifecycleCallbacks
 */
class Championship implements EntityIdentityInterface, EntityContentInterface
{
    use EntityIdentityTrait {
        EntityIdentityTrait::__construct as private __eitConstruct;
    }
    use EntityContentTrait;
    use TimestampableTrait;

    /**
     * @var ChampionshipRule
     *
     * @ORM\ManyToOne(targetEntity="ChampionshipRule", inversedBy="championships")
     * @ORM\JoinColumns({
     *     @ORM\JoinColumn(name="championship_rule_id", referencedColumnName="id")
     * })
     */
    private $championshipRule;

    /**
     * @ORM\OneToMany(targetEntity="Stage", mappedBy="championship")
     */
    private $stages;

    /**
     * @ORM\OneToMany(targetEntity="ChampionshipCompetitor", mappedBy="championship")
     */
    private $championshipCompetitors;

    public function __construct()
    {
        $this->__eitConstruct();
        $this->stages = new ArrayCollection();
        $this->championshipCompetitors = new ArrayCollection();
    }

    public function getChampionshipRule(): ?ChampionshipRule
    {
        return $this->championshipRule;
    }

    public function setChampionshipRule(?ChampionshipRule $championshipRule): self
    {
        $this->championshipRule = $championshipRule;

        return $this;
    }

    public function getStages(): Collection
    {
        return $this->stages;
    }

    public function addStage(Stage $stage): self
    {
        $stage->setChampionship($this);
        $this->stages->set($stage->getUuid(), $stage);

        return $this;
    }

    public function removeStage(Stage $stage): self
    {
        $this->stages->removeElement($stage);
        $stage->setChampionship(null);

        return $this;
    }

    /**
     * @return Collection|ChampionshipCompetitor[]
     */
    public function getChampionshipCompetitors(): Collection
    {
        return $this->championshipCompetitors;
    }

    public function addChampionshipCompetitor(ChampionshipCompetitor $championshipCompetitor): self
    {
        if (!$this->championshipCompetitors->contains($championshipCompetitor)) {
            $this->championshipCompetitors[] = $championshipCompetitor;
            $championshipCompetitor->setChampionship($this);
        }

        return $this;
    }

    public function removeChampionshipCompetitor(ChampionshipCompetitor $championshipCompetitor): self
    {
        if ($this->championshipCompetitors->contains($championshipCompetitor)) {
            $this->championshipCompetitors->removeElement($championshipCompetitor);
            if ($championshipCompetitor->getChampionship() === $this) {
                $championshipCompetitor->setChampionship(null);
            }
        }

        return $this;
    }
}
