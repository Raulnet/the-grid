<?php

namespace App\Entity;

use App\Entity\Interfaces\EntityContentInterface;
use App\Entity\Interfaces\EntityIdentityInterface;
use App\Entity\Interfaces\EntitySequenceCollectionInterface;
use App\Entity\Interfaces\EntitySequenceInterface;
use App\Entity\Traits\EntityContentTrait;
use App\Exception\GridException;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use App\Entity\Traits\EntityIdentityTrait;
use App\Entity\Traits\TimestampableTrait;

/**
 * Stage
 *
 * @ORM\Table(name="stage",
 *     uniqueConstraints={
 *         @ORM\UniqueConstraint(
 *             name="uuid_UNIQUE",
 *             columns={"uuid"}
 *         )
 *     },
 *     indexes={@ORM\Index(name="fk_stage_championship_idx", columns={"championship_id"})}
 * )
 * @ORM\Entity(repositoryClass="App\Repository\StageRepository")
 * @ORM\HasLifecycleCallbacks
 */
class Stage implements EntityIdentityInterface, EntityContentInterface, EntitySequenceCollectionInterface
{
    use EntityIdentityTrait {
        EntityIdentityTrait::__construct as private __eitConstruct;
    }
    use EntityContentTrait;
    use TimestampableTrait;

    /**
     * @var Championship
     *
     * @ORM\ManyToOne(targetEntity="Championship", inversedBy="stages")
     * @ORM\JoinColumns({
     *     @ORM\JoinColumn(name="championship_id", referencedColumnName="id")
     * })
     */
    private $championship;

    /**
     * @ORM\OneToMany(targetEntity="Round", mappedBy="stage")
     * @ORM\OrderBy({"sequence": "ASC"})
     */
    private $rounds;

    /**
     * @ORM\OneToMany(targetEntity="Lounge", mappedBy="stage")
     */
    private $lounges;

    /**
     * @ORM\OneToMany(targetEntity="StageCompetitor", mappedBy="stage")
     */
    private $stageCompetitors;

    public function __construct()
    {
        $this->__eitConstruct();
        $this->rounds = new ArrayCollection();
        $this->lounges = new ArrayCollection();
        $this->stageCompetitors = new ArrayCollection();
    }

    public function getChampionship(): ?Championship
    {
        return $this->championship;
    }

    public function setChampionship(?Championship $championship): self
    {
        $this->championship = $championship;

        return $this;
    }

    /**
     * @return Collection|Round[]
     */
    public function getRounds(): Collection
    {
        return $this->rounds;
    }

    public function addRound(Round $round): self
    {
        if (!$this->rounds->contains($round)) {
            $round->setSequence($this->rounds->count() + 1);
            $this->rounds[] = $round;
            $round->setStage($this);
        }

        return $this;
    }

    public function removeRound(Round $round): self
    {
        if ($this->rounds->contains($round)) {
            $this->rounds->removeElement($round);
            $round->setSequence(null);
            $round->setDateDeleted(new \DateTime('now'));
            if ($round->getStage() === $this) {
                $round->setStage(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|Lounge[]
     */
    public function getLounges(): Collection
    {
        return $this->lounges;
    }

    public function addLounge(Lounge $lounge): self
    {
        if (!$this->lounges->contains($lounge)) {
            $this->lounges[] = $lounge;
            $lounge->setStage($this);
        }

        return $this;
    }

    public function removeLounge(Lounge $lounge): self
    {
        if ($this->lounges->contains($lounge)) {
            $this->lounges->removeElement($lounge);
            $lounge->setDateDeleted(new \DateTime('now'));
            if ($lounge->getStage() === $this) {
                $lounge->setStage(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|StageCompetitor[]
     */
    public function getStageCompetitors(): Collection
    {
        return $this->stageCompetitors;
    }

    public function addStageCompetitor(StageCompetitor $stageCompetitor): self
    {
        if (!$this->stageCompetitors->contains($stageCompetitor)) {
            $this->stageCompetitors[] = $stageCompetitor;
            $stageCompetitor->setStage($this);
        }

        return $this;
    }

    public function removeStageCompetitor(StageCompetitor $stageCompetitor): self
    {
        if ($this->stageCompetitors->contains($stageCompetitor)) {
            $this->stageCompetitors->removeElement($stageCompetitor);
            if ($stageCompetitor->getStage() === $this) {
                $stageCompetitor->setStage(null);
            }
        }

        return $this;
    }

    public function _getSequenceCollection(): Collection
    {
        return $this->getRounds();
    }

    public function _removeSequence(EntitySequenceInterface $entitySequence): self
    {
        if (!$entitySequence instanceof Round) {
            throw new GridException('instance of Round required');
        }
        $this->removeRound($entitySequence);

        return $this;
    }
}
