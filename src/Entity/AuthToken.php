<?php

namespace App\Entity;

use App\Entity\Traits\EntityIdentityTrait;
use App\Entity\Traits\TimestampableTrait;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Table(name="auth_token",
 *     uniqueConstraints={
 *         @ORM\UniqueConstraint(
 *             name="uuid_UNIQUE",
 *             columns={"uuid"}
 *         )
 *     }
 * )
 * @ORM\Entity(repositoryClass="App\Repository\AuthTokenRepository")
 * @ORM\HasLifecycleCallbacks
 */
class AuthToken
{
    use EntityIdentityTrait {
        EntityIdentityTrait::__construct as private __eitConstruct;
    }
    use TimestampableTrait;

    /**
     * @ORM\Column(type="text")
     */
    private $token;

    /**
     * @var User;
     * @ORM\ManyToOne(targetEntity="App\Entity\User", inversedBy="authTokens")
     * @ORM\JoinColumns({
     *     @ORM\JoinColumn(name="user_id", referencedColumnName="id")
     * })
     */
    private $user;

    /**
     * @ORM\Column(name="expiration_time", type="datetime", nullable=false)
     */
    private $expirationTime;

    public function __construct()
    {
        $this->__eitConstruct();
        $this->expirationTime = new \DateTime('now');
        $this->expirationTime->modify('+90min');
    }

    public function getToken(): ?string
    {
        return $this->token;
    }

    public function setToken(string $token): self
    {
        $this->token = $token;

        return $this;
    }

    public function getUser(): ?User
    {
        return $this->user;
    }

    public function setUser(?User $user): self
    {
        $this->user = $user;

        return $this;
    }

    public function __toArray(): array
    {
        if ($this->isDeleted() || null === $this->getUser() || $this->getUser()->isDeleted()) {
            return [];
        }

        return [
            'iss' => $this->uuid,
            'sub' => $this->user->getUuid(),
            'data' => [
                'user' => $this->user->getUsername(),
            ],
            'token' => $this->getToken(),
            'exp' => $this->expirationTime->getTimestamp(),
        ];
    }

    public function getExpirationTime(): ?\DateTimeInterface
    {
        return $this->expirationTime;
    }

    public function setExpirationTime(\DateTimeInterface $expirationTime): self
    {
        $this->expirationTime = $expirationTime;

        return $this;
    }
}
