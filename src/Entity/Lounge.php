<?php

namespace App\Entity;

use App\Entity\Interfaces\EntityContentInterface;
use App\Entity\Interfaces\EntityIdentityInterface;
use App\Entity\Traits\EntityContentTrait;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use App\Entity\Traits\EntityIdentityTrait;
use App\Entity\Traits\TimestampableTrait;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * Lounge
 *
 * @ORM\Table(name="lounge",
 *     uniqueConstraints={
 *         @ORM\UniqueConstraint(
 *             name="uuid_UNIQUE",
 *             columns={"uuid"}
 *         )
 *     },
 *     indexes={@ORM\Index(name="fk_lounge_stage1_idx", columns={"stage_id"})}
 * )
 * @ORM\Entity(repositoryClass="App\Repository\LoungeRepository")
 * @ORM\HasLifecycleCallbacks
 */
class Lounge implements EntityIdentityInterface, EntityContentInterface
{
    use EntityIdentityTrait {
        EntityIdentityTrait::__construct as private __eitConstruct;
    }
    use EntityContentTrait;
    use TimestampableTrait;

    /**
     * @ORM\Column(name="coefficient", type="float", precision=10, scale=0, nullable=false, options={"default": 1.00})
     * @Assert\NotBlank(message="constraint.exception.coefficient.could.not.be.empty")
     */
    private $coefficient;

    /**
     * @var Stage
     *
     * @ORM\ManyToOne(targetEntity="Stage", inversedBy="lounges")
     * @ORM\JoinColumns({
     *     @ORM\JoinColumn(name="stage_id", referencedColumnName="id")
     * })
     */
    private $stage;

    /**
     * @ORM\OneToMany(targetEntity="LoungeCompetitor", mappedBy="lounge")
     */
    private $loungeCompetitors;

    public function __construct()
    {
        $this->__eitConstruct();
        $this->coefficient = 1.00;
        $this->loungeCompetitors = new ArrayCollection();
    }

    public function getCoefficient(): ?float
    {
        return $this->coefficient;
    }

    public function setCoefficient(float $coefficient): self
    {
        $this->coefficient = $coefficient;

        return $this;
    }

    public function getStage(): ?Stage
    {
        return $this->stage;
    }

    public function setStage(?Stage $stage): self
    {
        $this->stage = $stage;

        return $this;
    }

    /**
     * @return Collection|LoungeCompetitor[]
     */
    public function getLoungeCompetitors(): Collection
    {
        return $this->loungeCompetitors;
    }

    public function addLoungeCompetitor(LoungeCompetitor $loungeCompetitor): self
    {
        if (!$this->loungeCompetitors->contains($loungeCompetitor)) {
            $this->loungeCompetitors[] = $loungeCompetitor;
            $loungeCompetitor->setLounge($this);
        }

        return $this;
    }

    public function removeLoungeCompetitor(LoungeCompetitor $loungeCompetitor): self
    {
        if ($this->loungeCompetitors->contains($loungeCompetitor)) {
            $this->loungeCompetitors->removeElement($loungeCompetitor);
            if ($loungeCompetitor->getLounge() === $this) {
                $loungeCompetitor->setLounge(null);
            }
        }

        return $this;
    }
}
