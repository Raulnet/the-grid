<?php

namespace App\Entity;

use App\Entity\Interfaces\EntityIdentityInterface;
use App\Entity\Traits\EntityCompetitorTrait;
use Doctrine\ORM\Mapping as ORM;
use App\Entity\Traits\EntityIdentityTrait;
use App\Entity\Traits\TimestampableTrait;

/**
 * LoungeCompetitor
 *
 * @ORM\Table(
 *     name="lounge_competitor",
 *     indexes={
 *         @ORM\Index(name="fk_table1_competitor1_idx", columns={"competitor_id"}),
 *         @ORM\Index(name="fk_table1_lounge1_idx", columns={"lounge_id"}),
 *         @ORM\Index(name="fk_table1_country1_idx", columns={"country"}),
 *         @ORM\Index(name="fk_lounge_competitor_club1_idx", columns={"club_id"})
 *     },
 *     uniqueConstraints={
 *         @ORM\UniqueConstraint(
 *             name="competitor_has_joined_lounge",
 *             columns={"competitor_id", "lounge_id"}
 *         ),
 *         @ORM\UniqueConstraint(
 *             name="uuid_UNIQUE",
 *             columns={"uuid"}
 *         )
 *     }
 * )
 * @ORM\Entity(repositoryClass="App\Repository\LoungeCompetitorRepository")
 * @ORM\HasLifecycleCallbacks
 */
class LoungeCompetitor implements EntityIdentityInterface
{
    use EntityIdentityTrait;
    use TimestampableTrait;
    use EntityCompetitorTrait;

    /**
     * @var Lounge
     *
     * @ORM\ManyToOne(targetEntity="Lounge", inversedBy="loungeCompetitors")
     * @ORM\JoinColumns({
     *     @ORM\JoinColumn(name="lounge_id", referencedColumnName="id")
     * })
     */
    private $lounge;

    /**
     * @ORM\ManyToOne(targetEntity="StageCompetitor", inversedBy="loungeCompetitors")
     * @ORM\JoinColumns({
     *     @ORM\JoinColumn(name="stage_competitor_id", referencedColumnName="id", nullable=false)
     * })
     */
    private $stageCompetitor;

    public function getLounge(): Lounge
    {
        return $this->lounge;
    }

    public function setLounge(Lounge $lounge): self
    {
        $this->lounge = $lounge;

        return $this;
    }

    public function getStageCompetitor(): ?StageCompetitor
    {
        return $this->stageCompetitor;
    }

    public function setStageCompetitor(?StageCompetitor $stageCompetitor): self
    {
        $this->stageCompetitor = $stageCompetitor;
        if (null !== $stageCompetitor) {
            $this->setCompetitor($stageCompetitor->getCompetitor());
        }

        return $this;
    }
}
