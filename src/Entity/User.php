<?php

namespace App\Entity;

use App\Entity\Interfaces\EntityIdentityInterface;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use App\Entity\Traits\EntityIdentityTrait;
use App\Entity\Traits\TimestampableTrait;
use Ramsey\Uuid\Uuid;
use Symfony\Component\Security\Core\User\UserInterface;
use Symfony\Component\Validator\Constraints as Assert;
use Swagger\Annotations as SWG;

/**
 * User
 *
 * @ORM\Table(name="user",
 *     uniqueConstraints={
 *         @ORM\UniqueConstraint(
 *             name="uuid_UNIQUE",
 *             columns={"uuid"}
 *         ),
 *         @ORM\UniqueConstraint(
 *             name="api_key_UNIQUE",
 *             columns={"api_key"}
 *         )
 *     }
 * )
 * @ORM\Entity(repositoryClass="App\Repository\UserRepository")
 * @ORM\HasLifecycleCallbacks
 */
class User implements UserInterface, EntityIdentityInterface
{
    use EntityIdentityTrait {
        EntityIdentityTrait::__construct as private __eitConstruct;
    }
    use TimestampableTrait;

    /**
     * @ORM\Column(type="string", length=255, nullable=false)
     * @Assert\NotBlank(message="constraint.exception.username.could.not.be.empty")
     * @SWG\Property(type="string", maxLength=255)
     */
    private $username;

    /**
     * @ORM\Column(type="string", length=255, nullable=false)
     * @Assert\NotBlank(message="constraint.exception.password.could.not.be.empty")
     */
    private $password;

    /**
     * @ORM\Column(type="string", length=255, nullable=false)
     * @Assert\NotBlank(message="constraint.exception.salt.could.not.be.empty")
     */
    private $salt;

    /**
     * @ORM\Column(type="string", length=255, nullable=false)
     * @Assert\Email(message="constraint.exception.email.not.valid")
     * @Assert\NotBlank(message="constraint.exception.email.could.not.be.empty")
     */
    private $email;

    /**
     * @ORM\Column(type="array", nullable=false)
     */
    private $roles;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\AuthToken", mappedBy="user")
     */
    private $authTokens;

    /**
     * @ORM\Column(type="uuid")
     * @Assert\Uuid(message="constraint.exception.uuid.pattern.could.be.respected")
     */
    private $apiKey;

    public function __construct()
    {
        $this->__eitConstruct();
        $this->roles = ['ROLE_USER'];
        $this->salt = base_convert(sha1(md5(uniqid(mt_rand(), true))), 16, 36);
        $this->authTokens = new ArrayCollection();
        $this->apiKey = Uuid::uuid4();
    }

    public function getRoles(): array
    {
        return $this->roles;
    }

    public function setRoles(array $roles): void
    {
        $this->roles = $roles;
    }

    public function addRole(string $role): void
    {
        if (!\in_array($role, $this->roles, false)) {
            $this->roles[] = $role;
        }
    }

    public function removeRole(string $role): void
    {
        if (\in_array($role, $this->roles, false)) {
            $key = array_search($role, $this->roles, null);
            unset($this->roles[$key]);
        }
    }

    public function hasRole(string $role): bool
    {
        return \in_array($role, $this->roles, false);
    }

    public function getPassword(): ?string
    {
        return $this->password;
    }

    public function getSalt(): string
    {
        return $this->salt;
    }

    public function getUsername(): ?string
    {
        return $this->username;
    }

    public function eraseCredentials(): void
    {
    }

    public function __toString(): string
    {
        return $this->getUsername() ?: '';
    }

    public function setUsername(string $username): self
    {
        $this->username = $username;

        return $this;
    }

    public function setPassword(string $password): self
    {
        $this->password = $password;

        return $this;
    }

    public function setSalt(string $salt): self
    {
        $this->salt = $salt;

        return $this;
    }

    public function getEmail(): ?string
    {
        return $this->email;
    }

    public function setEmail(string $email): self
    {
        $this->email = $email;

        return $this;
    }

    /**
     * @return Collection|AuthToken[]
     */
    public function getAuthTokens(): Collection
    {
        return $this->authTokens;
    }

    public function addAuthToken(AuthToken $authToken): self
    {
        if (!$this->authTokens->contains($authToken)) {
            $this->authTokens->add($authToken);
            $authToken->setUser($this);
        }

        return $this;
    }

    public function removeAuthToken(AuthToken $authToken): self
    {
        if ($this->authTokens->contains($authToken)) {
            $this->authTokens->removeElement($authToken);
            if ($authToken->getUser() === $this) {
                $authToken->setUser(null);
            }
        }

        return $this;
    }

    public function getApiKey(): string
    {
        return $this->apiKey;
    }

    public function setApiKey(string $apiKey): self
    {
        $this->apiKey = $apiKey;

        return $this;
    }
}
