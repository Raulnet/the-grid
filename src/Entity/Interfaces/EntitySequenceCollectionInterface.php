<?php

namespace App\Entity\Interfaces;

use Doctrine\Common\Collections\Collection;

interface EntitySequenceCollectionInterface
{
    public function _getSequenceCollection(): Collection;

    public function _removeSequence(EntitySequenceInterface $entitySequence);
}
