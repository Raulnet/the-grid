<?php
/**
 * Created by PhpStorm.
 * User: raulnet
 * Date: 01/01/19
 * Time: 22:08
 */

namespace App\Entity\Interfaces;

interface EntityContentInterface
{
    public function getTitle(): ?string;

    public function setTitle(string $title);

    public function getDescription(): ?string;

    public function setDescription(string $description);

    public function __toString(): string;
}
