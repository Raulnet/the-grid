<?php

namespace App\Entity\Interfaces;

interface EntitySequenceInterface
{
    public function getSequence(): ?int;

    public function setSequence(?int $sequence);

    public function getDateStart(): ?\DateTime;

    public function setDateStart(?\DateTime $startDate);

    public function getDateEnd(): ?\DateTime;

    public function setDateEnd(?\DateTime $endDate);

    public function getTimeZone(): string;

    public function setTimeZone(string $timeZone);

    public function getClassName(): string;
}
