<?php
/**
 * Created by PhpStorm.
 * User: raulnet
 * Date: 01/01/19
 * Time: 22:06
 */

namespace App\Entity\Interfaces;

interface EntityIdentityInterface
{
    public function getId(): ?int;

    public function getUuid(): string;
}
