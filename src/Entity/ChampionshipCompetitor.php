<?php

namespace App\Entity;

use App\Entity\Interfaces\EntityIdentityInterface;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use App\Entity\Traits\EntityIdentityTrait;
use App\Entity\Traits\TimestampableTrait;
use App\Entity\Traits\EntityCompetitorTrait;

/**
 * @ORM\Table(
 *     name="championship_competitor",
 *     indexes={
 *         @ORM\Index(name="fk_championship_competitor_competitor1_idx", columns={"competitor_id"}),
 *         @ORM\Index(name="fk_championship_competitor_club1_idx", columns={"club_id"}),
 *         @ORM\Index(name="fk_championship_competitor_country1_idx", columns={"country"}),
 *         @ORM\Index(name="fk_championship_competitor_championship1_idx", columns={"championship_id"})
 *     },
 *     uniqueConstraints={
 *         @ORM\UniqueConstraint(
 *             name="competitor_has_joined_championship",
 *             columns={"competitor_id", "championship_id"}
 *         ),
 *         @ORM\UniqueConstraint(
 *             name="uuid_UNIQUE",
 *             columns={"uuid"}
 *         )
 *     }
 * )
 * @ORM\Entity(repositoryClass="App\Repository\ChampionshipCompetitorRepository")
 * @ORM\HasLifecycleCallbacks
 */
class ChampionshipCompetitor implements EntityIdentityInterface
{
    use EntityIdentityTrait {
        EntityIdentityTrait::__construct as private __eitConstruct;
    }
    use EntityCompetitorTrait;
    use TimestampableTrait;

    /**
     * @var Championship
     *
     * @ORM\ManyToOne(targetEntity="Championship", inversedBy="championshipCompetitors")
     * @ORM\JoinColumns({
     *     @ORM\JoinColumn(name="championship_id", referencedColumnName="id")
     * })
     */
    private $championship;

    /**
     * @ORM\OneToMany(targetEntity="StageCompetitor", mappedBy="championshipCompetitor")
     */
    private $stageCompetitors;

    public function __construct()
    {
        $this->__eitConstruct();
        $this->stageCompetitors = new ArrayCollection();
    }

    public function getChampionship(): ?Championship
    {
        return $this->championship;
    }

    public function setChampionship(?Championship $championship): self
    {
        $this->championship = $championship;

        return $this;
    }

    /**
     * @return Collection|StageCompetitor[]
     */
    public function getStageCompetitors(): Collection
    {
        return $this->stageCompetitors;
    }

    public function addStageCompetitor(StageCompetitor $stageCompetitor): self
    {
        if (!$this->stageCompetitors->contains($stageCompetitor)) {
            $this->stageCompetitors[] = $stageCompetitor;
            $stageCompetitor->setChampionshipCompetitor($this);
        }

        return $this;
    }

    public function removeStageCompetitor(StageCompetitor $stageCompetitor): self
    {
        if ($this->stageCompetitors->contains($stageCompetitor)) {
            $this->stageCompetitors->removeElement($stageCompetitor);
            if ($stageCompetitor->getChampionshipCompetitor() === $this) {
                $stageCompetitor->setChampionshipCompetitor(null);
            }
        }

        return $this;
    }
}
