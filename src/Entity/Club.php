<?php

namespace App\Entity;

use App\Entity\Interfaces\EntityContentInterface;
use App\Entity\Interfaces\EntityIdentityInterface;
use App\Entity\Traits\EntityContentTrait;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
use App\Entity\Traits\EntityIdentityTrait;
use App\Entity\Traits\TimestampableTrait;
use Doctrine\Common\Collections\Collection;

/**
 * Club
 *
 * @ORM\Table(name="club",
 *     uniqueConstraints={
 *         @ORM\UniqueConstraint(
 *             name="uuid_UNIQUE",
 *             columns={"uuid"}
 *         )
 *     }
 * )
 * @ORM\Entity(repositoryClass="App\Repository\ClubRepository")
 * @ORM\HasLifecycleCallbacks
 */
class Club implements EntityIdentityInterface, EntityContentInterface
{
    use EntityIdentityTrait {
        EntityIdentityTrait::__construct as private __eitConstruct;
    }
    use EntityContentTrait;
    use TimestampableTrait;

    /**
     * @ORM\OneToMany(targetEntity="Competitor", mappedBy="club")
     */
    private $competitors;

    public function __construct()
    {
        $this->__eitConstruct();
        $this->competitors = new ArrayCollection();
    }

    /**
     * @return Collection|Competitor[]
     */
    public function getCompetitors(): Collection
    {
        return $this->competitors;
    }

    public function addCompetitor(Competitor $competitor): self
    {
        if (!$this->competitors->contains($competitor)) {
            $this->competitors[] = $competitor;
            $competitor->setClub($this);
        }

        return $this;
    }

    public function removeCompetitor(Competitor $competitor): self
    {
        if ($this->competitors->contains($competitor)) {
            $this->competitors->removeElement($competitor);
            if ($competitor->getClub() === $this) {
                $competitor->setClub(null);
            }
        }

        return $this;
    }
}
