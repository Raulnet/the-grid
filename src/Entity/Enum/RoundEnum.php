<?php

namespace App\Entity\Enum;

class RoundEnum
{
    public const PLACE_BUILD = 'build';
    public const PLACE_START = 'start';
    public const PLACE_IN_PROGRESS = 'in_progress';
    public const PLACE_CLOSED = 'closed';
    public const PLACE_IN_REVISION = 'in_revision';
    public const PLACE_DELETED = 'deleted';

    public const TRANSITION_LAUNCH = 'launch';
    public const TRANSITION_REMOVE = 'remove';
    public const TRANSITION_EDIT = 'edit';
    public const TRANSITION_SUBMIT = 'submit';
    public const TRANSITION_REJECT = 'reject';
    public const TRANSITION_REOPEN = 'reopen';

    public const listPlaces = [
            self::PLACE_BUILD,
            self::PLACE_START,
            self::PLACE_IN_PROGRESS,
            self::PLACE_CLOSED,
            self::PLACE_IN_REVISION,
            self::PLACE_DELETED,
        ];
}
