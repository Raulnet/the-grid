<?php

namespace App\EventListener;

use JsonSchema\Exception\ValidationException;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Event\GetResponseForExceptionEvent;
use Symfony\Component\HttpKernel\Exception\HttpExceptionInterface;

class ExceptionListener
{
    public function onKernelException(GetResponseForExceptionEvent $event): void
    {
        // You get the exception object from the received event
        $exception = $event->getException();
        switch (true) {
            case $exception instanceof HttpExceptionInterface:
                $response = $this->getHttpExceptionResponse($exception);
                break;
            case $exception instanceof ValidationException:
                $response = $this->getValidationExceptionResponse($exception);
                break;
            default:
                $response = new JsonResponse();
                $data = [
                    'code' => $exception->getCode(),
                    'message' => $exception->getMessage(),
                ];
                $response->setData($data);
                $response->setStatusCode(0 === $exception->getCode() ? JsonResponse::HTTP_INTERNAL_SERVER_ERROR : $exception->getCode());
        }

        $event->setResponse($response);
    }

    private function getHttpExceptionResponse(HttpExceptionInterface $e): Response
    {
        $data = [
            'code' => $e->getStatusCode(),
        ];
        $response = new JsonResponse($data);
        $response->setStatusCode($e->getStatusCode());
        $response->headers->replace($e->getHeaders());

        return $response;
    }

    private function getValidationExceptionResponse(ValidationException $e): Response
    {
        $data = [
            'code' => $e->getCode(),
            'message' => $e->getMessage(),
        ];
        $response = new JsonResponse($data);
        $response->setStatusCode($e->getCode());

        return $response;
    }
}
