<?php

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

final class Version20190126210321 extends AbstractMigration
{
    public function up(Schema $schema): void
    {
        $this->addSql('CREATE TABLE round (id INT UNSIGNED AUTO_INCREMENT NOT NULL, stage_id INT UNSIGNED DEFAULT NULL, current_place VARCHAR(10) NOT NULL, uuid CHAR(36) NOT NULL COMMENT \'(DC2Type:uuid)\', title VARCHAR(80) NOT NULL, description LONGTEXT NOT NULL, date_creation DATETIME NOT NULL, date_update DATETIME NOT NULL, date_deleted DATETIME DEFAULT NULL, sequence INT DEFAULT NULL, date_start DATETIME DEFAULT NULL, date_end DATETIME DEFAULT NULL, time_zone VARCHAR(255) NOT NULL, INDEX fk_round_stage1_idx (stage_id), UNIQUE INDEX uuid_UNIQUE (uuid), UNIQUE INDEX round_sequence_UNIQUE (sequence, stage_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE lounge (id INT UNSIGNED AUTO_INCREMENT NOT NULL, stage_id INT UNSIGNED DEFAULT NULL, coefficient DOUBLE PRECISION DEFAULT \'1\' NOT NULL, uuid CHAR(36) NOT NULL COMMENT \'(DC2Type:uuid)\', title VARCHAR(80) NOT NULL, description LONGTEXT NOT NULL, date_creation DATETIME NOT NULL, date_update DATETIME NOT NULL, date_deleted DATETIME DEFAULT NULL, INDEX fk_lounge_stage1_idx (stage_id), UNIQUE INDEX uuid_UNIQUE (uuid), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE lounge_competitor (id INT UNSIGNED AUTO_INCREMENT NOT NULL, lounge_id INT UNSIGNED DEFAULT NULL, stage_competitor_id INT UNSIGNED NOT NULL, club_id INT UNSIGNED DEFAULT NULL, competitor_id INT UNSIGNED DEFAULT NULL, uuid CHAR(36) NOT NULL COMMENT \'(DC2Type:uuid)\', date_creation DATETIME NOT NULL, date_update DATETIME NOT NULL, date_deleted DATETIME DEFAULT NULL, score INT DEFAULT NULL, country VARCHAR(3) DEFAULT NULL, INDEX IDX_C14C27F1845C5285 (stage_competitor_id), INDEX fk_table1_competitor1_idx (competitor_id), INDEX fk_table1_lounge1_idx (lounge_id), INDEX fk_table1_country1_idx (country), INDEX fk_lounge_competitor_club1_idx (club_id), UNIQUE INDEX competitor_has_joined_lounge (competitor_id, lounge_id), UNIQUE INDEX uuid_UNIQUE (uuid), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE stage (id INT UNSIGNED AUTO_INCREMENT NOT NULL, championship_id INT UNSIGNED DEFAULT NULL, uuid CHAR(36) NOT NULL COMMENT \'(DC2Type:uuid)\', title VARCHAR(80) NOT NULL, description LONGTEXT NOT NULL, date_creation DATETIME NOT NULL, date_update DATETIME NOT NULL, date_deleted DATETIME DEFAULT NULL, INDEX fk_stage_championship_idx (championship_id), UNIQUE INDEX uuid_UNIQUE (uuid), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE performance (id INT UNSIGNED AUTO_INCREMENT NOT NULL, lounge_competitor_id INT UNSIGNED DEFAULT NULL, round_id INT UNSIGNED DEFAULT NULL, club_id INT UNSIGNED DEFAULT NULL, competitor_id INT UNSIGNED DEFAULT NULL, performance INT DEFAULT NULL, uuid CHAR(36) NOT NULL COMMENT \'(DC2Type:uuid)\', date_creation DATETIME NOT NULL, date_update DATETIME NOT NULL, date_deleted DATETIME DEFAULT NULL, score INT DEFAULT NULL, country VARCHAR(3) DEFAULT NULL, INDEX IDX_82D7968161190A32 (club_id), INDEX IDX_82D7968178A5D405 (competitor_id), INDEX fk_performance_lounge_competitor1_idx (lounge_competitor_id), INDEX fk_performance_country1_idx (country), INDEX fk_performance_round1_idx (round_id), UNIQUE INDEX competitor_has_joined_championship (competitor_id, round_id), UNIQUE INDEX uuid_UNIQUE (uuid), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE user (id INT UNSIGNED AUTO_INCREMENT NOT NULL, username VARCHAR(255) NOT NULL, password VARCHAR(255) NOT NULL, salt VARCHAR(255) NOT NULL, email VARCHAR(255) NOT NULL, roles LONGTEXT NOT NULL COMMENT \'(DC2Type:array)\', api_key CHAR(36) NOT NULL COMMENT \'(DC2Type:uuid)\', uuid CHAR(36) NOT NULL COMMENT \'(DC2Type:uuid)\', date_creation DATETIME NOT NULL, date_update DATETIME NOT NULL, date_deleted DATETIME DEFAULT NULL, UNIQUE INDEX uuid_UNIQUE (uuid), UNIQUE INDEX api_key_UNIQUE (api_key), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE auth_token (id INT UNSIGNED AUTO_INCREMENT NOT NULL, user_id INT UNSIGNED DEFAULT NULL, token LONGTEXT NOT NULL, expiration_time DATETIME NOT NULL, uuid CHAR(36) NOT NULL COMMENT \'(DC2Type:uuid)\', date_creation DATETIME NOT NULL, date_update DATETIME NOT NULL, date_deleted DATETIME DEFAULT NULL, INDEX IDX_9315F04EA76ED395 (user_id), UNIQUE INDEX uuid_UNIQUE (uuid), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE club (id INT UNSIGNED AUTO_INCREMENT NOT NULL, uuid CHAR(36) NOT NULL COMMENT \'(DC2Type:uuid)\', title VARCHAR(80) NOT NULL, description LONGTEXT NOT NULL, date_creation DATETIME NOT NULL, date_update DATETIME NOT NULL, date_deleted DATETIME DEFAULT NULL, UNIQUE INDEX uuid_UNIQUE (uuid), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE championship_competitor (id INT UNSIGNED AUTO_INCREMENT NOT NULL, championship_id INT UNSIGNED DEFAULT NULL, club_id INT UNSIGNED DEFAULT NULL, competitor_id INT UNSIGNED DEFAULT NULL, uuid CHAR(36) NOT NULL COMMENT \'(DC2Type:uuid)\', score INT DEFAULT NULL, country VARCHAR(3) DEFAULT NULL, date_creation DATETIME NOT NULL, date_update DATETIME NOT NULL, date_deleted DATETIME DEFAULT NULL, INDEX fk_championship_competitor_competitor1_idx (competitor_id), INDEX fk_championship_competitor_club1_idx (club_id), INDEX fk_championship_competitor_country1_idx (country), INDEX fk_championship_competitor_championship1_idx (championship_id), UNIQUE INDEX competitor_has_joined_championship (competitor_id, championship_id), UNIQUE INDEX uuid_UNIQUE (uuid), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE championship (id INT UNSIGNED AUTO_INCREMENT NOT NULL, championship_rule_id INT UNSIGNED DEFAULT NULL, uuid CHAR(36) NOT NULL COMMENT \'(DC2Type:uuid)\', title VARCHAR(80) NOT NULL, description LONGTEXT NOT NULL, date_creation DATETIME NOT NULL, date_update DATETIME NOT NULL, date_deleted DATETIME DEFAULT NULL, INDEX fk_championship_championship_rule1_idx (championship_rule_id), UNIQUE INDEX uuid_UNIQUE (uuid), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE competitor (id INT UNSIGNED AUTO_INCREMENT NOT NULL, club_id INT UNSIGNED DEFAULT NULL, username VARCHAR(140) NOT NULL, country VARCHAR(3) NOT NULL, bid VARCHAR(12) NOT NULL, uuid CHAR(36) NOT NULL COMMENT \'(DC2Type:uuid)\', date_creation DATETIME NOT NULL, date_update DATETIME NOT NULL, date_deleted DATETIME DEFAULT NULL, INDEX fk_competitor_club1_idx (club_id), UNIQUE INDEX uuid_UNIQUE (uuid), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE stage_competitor (id INT UNSIGNED AUTO_INCREMENT NOT NULL, championship_competitor_id INT UNSIGNED NOT NULL, stage_id INT UNSIGNED DEFAULT NULL, club_id INT UNSIGNED DEFAULT NULL, competitor_id INT UNSIGNED DEFAULT NULL, uuid CHAR(36) NOT NULL COMMENT \'(DC2Type:uuid)\', date_creation DATETIME NOT NULL, date_update DATETIME NOT NULL, date_deleted DATETIME DEFAULT NULL, score INT DEFAULT NULL, country VARCHAR(3) DEFAULT NULL, INDEX IDX_AC8F200E38544E5A (championship_competitor_id), INDEX fk_stage_competitor_stage1_idx (stage_id), INDEX fk_stage_competitor_competitor1_idx (competitor_id), INDEX fk_stage_competitor_country1_idx (country), INDEX fk_stage_competitor_club1_idx (club_id), UNIQUE INDEX competitor_has_joined_stage (competitor_id, stage_id), UNIQUE INDEX uuid_UNIQUE (uuid), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE championship_rule (id INT UNSIGNED AUTO_INCREMENT NOT NULL, rules LONGTEXT NOT NULL, uuid CHAR(36) NOT NULL COMMENT \'(DC2Type:uuid)\', title VARCHAR(80) NOT NULL, description LONGTEXT NOT NULL, date_creation DATETIME NOT NULL, date_update DATETIME NOT NULL, date_deleted DATETIME DEFAULT NULL, UNIQUE INDEX uuid_UNIQUE (uuid), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB');
        $this->addSql('ALTER TABLE round ADD CONSTRAINT FK_C5EEEA342298D193 FOREIGN KEY (stage_id) REFERENCES stage (id)');
        $this->addSql('ALTER TABLE lounge ADD CONSTRAINT FK_DC5A502D2298D193 FOREIGN KEY (stage_id) REFERENCES stage (id)');
        $this->addSql('ALTER TABLE lounge_competitor ADD CONSTRAINT FK_C14C27F167D1F5E1 FOREIGN KEY (lounge_id) REFERENCES lounge (id)');
        $this->addSql('ALTER TABLE lounge_competitor ADD CONSTRAINT FK_C14C27F1845C5285 FOREIGN KEY (stage_competitor_id) REFERENCES stage_competitor (id)');
        $this->addSql('ALTER TABLE lounge_competitor ADD CONSTRAINT FK_C14C27F161190A32 FOREIGN KEY (club_id) REFERENCES club (id)');
        $this->addSql('ALTER TABLE lounge_competitor ADD CONSTRAINT FK_C14C27F178A5D405 FOREIGN KEY (competitor_id) REFERENCES competitor (id)');
        $this->addSql('ALTER TABLE stage ADD CONSTRAINT FK_C27C936994DDBCE9 FOREIGN KEY (championship_id) REFERENCES championship (id)');
        $this->addSql('ALTER TABLE performance ADD CONSTRAINT FK_82D7968177E863C8 FOREIGN KEY (lounge_competitor_id) REFERENCES lounge_competitor (id)');
        $this->addSql('ALTER TABLE performance ADD CONSTRAINT FK_82D79681A6005CA0 FOREIGN KEY (round_id) REFERENCES round (id)');
        $this->addSql('ALTER TABLE performance ADD CONSTRAINT FK_82D7968161190A32 FOREIGN KEY (club_id) REFERENCES club (id)');
        $this->addSql('ALTER TABLE performance ADD CONSTRAINT FK_82D7968178A5D405 FOREIGN KEY (competitor_id) REFERENCES competitor (id)');
        $this->addSql('ALTER TABLE auth_token ADD CONSTRAINT FK_9315F04EA76ED395 FOREIGN KEY (user_id) REFERENCES user (id)');
        $this->addSql('ALTER TABLE championship_competitor ADD CONSTRAINT FK_BB6CE61894DDBCE9 FOREIGN KEY (championship_id) REFERENCES championship (id)');
        $this->addSql('ALTER TABLE championship_competitor ADD CONSTRAINT FK_BB6CE61861190A32 FOREIGN KEY (club_id) REFERENCES club (id)');
        $this->addSql('ALTER TABLE championship_competitor ADD CONSTRAINT FK_BB6CE61878A5D405 FOREIGN KEY (competitor_id) REFERENCES competitor (id)');
        $this->addSql('ALTER TABLE championship ADD CONSTRAINT FK_EBADDE6A4057D2B9 FOREIGN KEY (championship_rule_id) REFERENCES championship_rule (id)');
        $this->addSql('ALTER TABLE competitor ADD CONSTRAINT FK_E0D53BAA61190A32 FOREIGN KEY (club_id) REFERENCES club (id)');
        $this->addSql('ALTER TABLE stage_competitor ADD CONSTRAINT FK_AC8F200E38544E5A FOREIGN KEY (championship_competitor_id) REFERENCES championship_competitor (id)');
        $this->addSql('ALTER TABLE stage_competitor ADD CONSTRAINT FK_AC8F200E2298D193 FOREIGN KEY (stage_id) REFERENCES stage (id)');
        $this->addSql('ALTER TABLE stage_competitor ADD CONSTRAINT FK_AC8F200E61190A32 FOREIGN KEY (club_id) REFERENCES club (id)');
        $this->addSql('ALTER TABLE stage_competitor ADD CONSTRAINT FK_AC8F200E78A5D405 FOREIGN KEY (competitor_id) REFERENCES competitor (id)');
    }

    public function down(Schema $schema): void
    {
        $this->addSql('ALTER TABLE performance DROP FOREIGN KEY FK_82D79681A6005CA0');
        $this->addSql('ALTER TABLE lounge_competitor DROP FOREIGN KEY FK_C14C27F167D1F5E1');
        $this->addSql('ALTER TABLE performance DROP FOREIGN KEY FK_82D7968177E863C8');
        $this->addSql('ALTER TABLE round DROP FOREIGN KEY FK_C5EEEA342298D193');
        $this->addSql('ALTER TABLE lounge DROP FOREIGN KEY FK_DC5A502D2298D193');
        $this->addSql('ALTER TABLE stage_competitor DROP FOREIGN KEY FK_AC8F200E2298D193');
        $this->addSql('ALTER TABLE auth_token DROP FOREIGN KEY FK_9315F04EA76ED395');
        $this->addSql('ALTER TABLE lounge_competitor DROP FOREIGN KEY FK_C14C27F161190A32');
        $this->addSql('ALTER TABLE performance DROP FOREIGN KEY FK_82D7968161190A32');
        $this->addSql('ALTER TABLE championship_competitor DROP FOREIGN KEY FK_BB6CE61861190A32');
        $this->addSql('ALTER TABLE competitor DROP FOREIGN KEY FK_E0D53BAA61190A32');
        $this->addSql('ALTER TABLE stage_competitor DROP FOREIGN KEY FK_AC8F200E61190A32');
        $this->addSql('ALTER TABLE stage_competitor DROP FOREIGN KEY FK_AC8F200E38544E5A');
        $this->addSql('ALTER TABLE stage DROP FOREIGN KEY FK_C27C936994DDBCE9');
        $this->addSql('ALTER TABLE championship_competitor DROP FOREIGN KEY FK_BB6CE61894DDBCE9');
        $this->addSql('ALTER TABLE lounge_competitor DROP FOREIGN KEY FK_C14C27F178A5D405');
        $this->addSql('ALTER TABLE performance DROP FOREIGN KEY FK_82D7968178A5D405');
        $this->addSql('ALTER TABLE championship_competitor DROP FOREIGN KEY FK_BB6CE61878A5D405');
        $this->addSql('ALTER TABLE stage_competitor DROP FOREIGN KEY FK_AC8F200E78A5D405');
        $this->addSql('ALTER TABLE lounge_competitor DROP FOREIGN KEY FK_C14C27F1845C5285');
        $this->addSql('ALTER TABLE championship DROP FOREIGN KEY FK_EBADDE6A4057D2B9');
        $this->addSql('DROP TABLE round');
        $this->addSql('DROP TABLE lounge');
        $this->addSql('DROP TABLE lounge_competitor');
        $this->addSql('DROP TABLE stage');
        $this->addSql('DROP TABLE performance');
        $this->addSql('DROP TABLE user');
        $this->addSql('DROP TABLE auth_token');
        $this->addSql('DROP TABLE club');
        $this->addSql('DROP TABLE championship_competitor');
        $this->addSql('DROP TABLE championship');
        $this->addSql('DROP TABLE competitor');
        $this->addSql('DROP TABLE stage_competitor');
        $this->addSql('DROP TABLE championship_rule');
    }
}
