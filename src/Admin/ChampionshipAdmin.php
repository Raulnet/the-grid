<?php
/**
 * Created by PhpStorm.
 * User: raulnet
 * Date: 28/12/18
 * Time: 22:31
 */

namespace App\Admin;

use App\Entity\ChampionshipRule;
use Sonata\AdminBundle\Admin\AbstractAdmin;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Form\FormMapper;
use Sonata\AdminBundle\Form\Type\ModelType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\TextType;

class ChampionshipAdmin extends AbstractAdmin
{
    protected function configureFormFields(FormMapper $formMapper)
    {
        $formMapper->add('title', TextType::class)
        ->add('description', TextareaType::class)
        ->add('championshipRule', ModelType::class, [
            'class' => ChampionshipRule::class,
            'label' => 'Rule',
        ])
        ;
    }

    protected function configureDatagridFilters(DatagridMapper $datagridMapper)
    {
        $datagridMapper->add('title');
    }

    protected function configureListFields(ListMapper $listMapper)
    {
        $listMapper->addIdentifier('title')->add('description')->add('isDeleted');
    }
}
