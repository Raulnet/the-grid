<?php
/**
 * Created by PhpStorm.
 * User: raulnet
 * Date: 29/12/18
 * Time: 00:22
 */

namespace App\Admin;

use App\Entity\Championship;
use Sonata\AdminBundle\Admin\AbstractAdmin;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Form\FormMapper;
use Sonata\AdminBundle\Form\Type\ModelType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\TextType;

class StageAdmin extends AbstractAdmin
{
    protected function configureFormFields(FormMapper $formMapper)
    {
        $formMapper->add('title', TextType::class)
            ->add('description', TextareaType::class)
            ->add('championship', ModelType::class, [
                'class' => Championship::class,
                'label' => 'Championship',
            ])
        ;
    }

    protected function configureDatagridFilters(DatagridMapper $datagridMapper)
    {
        $datagridMapper
            ->add('championship')
            ->add('title')
        ;
    }

    protected function configureListFields(ListMapper $listMapper)
    {
        $listMapper
            ->add('championship')
            ->addIdentifier('title')
            ->add('description')
        ;
    }
}
