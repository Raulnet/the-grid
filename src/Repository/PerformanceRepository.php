<?php

namespace App\Repository;

use App\Entity\Lounge;
use App\Entity\Performance;
use App\Entity\Round;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Symfony\Bridge\Doctrine\RegistryInterface;

class PerformanceRepository extends ServiceEntityRepository
{
    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, Performance::class);
    }

    public function findPerformances(Lounge $lounge, ?Round $round = null): array
    {
        $qb = $this->createQueryBuilder('p');

        $qb->join('p.loungeCompetitor', 'loungeCompetitor')
            ->where('p.dateDeleted is NULL')
            ->andWhere('loungeCompetitor.lounge = :l')
        ;

        $parameters = ['l' => $lounge];

        if (null !== $round) {
            $qb->andWhere('p.round = :r');
            $parameters['r'] = $round;
        }

        $qb->setParameters($parameters);

        return $qb->getQuery()
            ->getResult()
        ;
    }
}
