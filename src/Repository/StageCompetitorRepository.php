<?php

namespace App\Repository;

use App\Entity\Competitor;
use App\Entity\Lounge;
use App\Entity\StageCompetitor;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Symfony\Bridge\Doctrine\RegistryInterface;

/**
 * @method StageCompetitor|null find($id, $lockMode = null, $lockVersion = null)
 * @method StageCompetitor|null findOneBy(array $criteria, array $orderBy = null)
 * @method StageCompetitor[]    findAll()
 * @method StageCompetitor[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class StageCompetitorRepository extends ServiceEntityRepository
{
    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, StageCompetitor::class);
    }

    public function findStageCompetitorByLounge(Lounge $lounge, Competitor $competitor): ?StageCompetitor
    {
        return $this->findOneBy([
            'competitor' => $competitor,
            'dateDeleted' => null,
            'stage' => $lounge->getStage(),
        ]);
    }
}
