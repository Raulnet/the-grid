<?php

namespace App\Repository;

use App\Entity\AuthToken;
use App\Entity\User;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Symfony\Bridge\Doctrine\RegistryInterface;

/**
 * @method AuthToken|null find($id, $lockMode = null, $lockVersion = null)
 * @method AuthToken|null findOneBy(array $criteria, array $orderBy = null)
 * @method AuthToken[]    findAll()
 * @method AuthToken[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class AuthTokenRepository extends ServiceEntityRepository
{
    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, AuthToken::class);
    }

    public function deleteAuthToken(User $user): void
    {
        $now = new \DateTime('now');
        $this->createQueryBuilder('auth_token')
            ->update()
            ->set('auth_token.dateDeleted', ':d')
            ->where('auth_token.user = :u')
            ->setParameters([
                ':d' => $now,
                ':u' => $user,
            ])
            ->getQuery()
            ->execute()
        ;
    }
}
