<?php

namespace App\Repository;

use App\Entity\ChampionshipCompetitor;
use App\Entity\Competitor;
use App\Entity\Stage;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Symfony\Bridge\Doctrine\RegistryInterface;

/**
 * @method ChampionshipCompetitor|null find($id, $lockMode = null, $lockVersion = null)
 * @method ChampionshipCompetitor|null findOneBy(array $criteria, array $orderBy = null)
 * @method ChampionshipCompetitor[]    findAll()
 * @method ChampionshipCompetitor[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class ChampionshipCompetitorRepository extends ServiceEntityRepository
{
    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, ChampionshipCompetitor::class);
    }

    public function findChampionshipCompetitorByStage(Stage $stage, Competitor $competitor): ?ChampionshipCompetitor
    {
        return $this->findOneBy([
            'competitor' => $competitor,
            'dateDeleted' => null,
            'championship' => $stage->getChampionship(),
        ]);
    }
}
