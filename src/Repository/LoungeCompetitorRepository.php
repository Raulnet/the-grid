<?php

namespace App\Repository;

use App\Entity\LoungeCompetitor;
use App\Entity\Round;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Symfony\Bridge\Doctrine\RegistryInterface;

class LoungeCompetitorRepository extends ServiceEntityRepository
{
    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, LoungeCompetitor::class);
    }

    public function findByRound(Round $round): iterable
    {
        return $this->createQueryBuilder('lc')
            ->join('lc.stageCompetitor', 'sc')
            ->where('sc.stage = :s')
            ->andWhere('lc.dateDeleted is NULL')
            ->andWhere('sc.dateDeleted is NULL')
            ->setParameters(['s' => $round->getStage()])
            ->getQuery()
            ->iterate()
        ;
    }
}
