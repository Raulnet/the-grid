<?php

namespace App\Repository;

use App\Entity\Championship;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Knp\Component\Pager\Pagination\PaginationInterface;
use Knp\Component\Pager\PaginatorInterface;
use Symfony\Bridge\Doctrine\RegistryInterface;

class ChampionshipRepository extends ServiceEntityRepository
{
    private const LIMIT = 10;
    private $paginator;

    public function __construct(RegistryInterface $registry, PaginatorInterface $paginator)
    {
        parent::__construct($registry, Championship::class);
        $this->paginator = $paginator;
    }

    public function paginate(int $page = 1): PaginationInterface
    {
        $query = $this->createQueryBuilder('c')->getQuery();

        return $this->paginator->paginate(
            $query,
            $page,
            self::LIMIT
        );
    }
}
