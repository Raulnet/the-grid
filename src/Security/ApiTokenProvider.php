<?php
/**
 * Created by PhpStorm.
 * User: raulnet
 * Date: 03/01/19
 * Time: 00:21
 */

namespace App\Security;

use App\Entity\AuthToken;
use App\Entity\User;
use Firebase\JWT\JWT;

class ApiTokenProvider
{
    private $tokenApiKey;

    public function __construct(string $tokenApiKey)
    {
        $this->tokenApiKey = $tokenApiKey;
    }

    public function getToken(AuthToken $authToken): string
    {
        return JWT::encode($authToken->__toArray(), $this->tokenApiKey);
    }

    public function getAuthToken(User $user): AuthToken
    {
        $authToken = new AuthToken();
        $authToken->setToken(base64_encode(random_bytes(512)));
        $authToken->setUser($user);

        return $authToken;
    }

    public function decodeToken(string $token): object
    {
        return JWT::decode($token, $this->tokenApiKey, ['HS256']);
    }
}
