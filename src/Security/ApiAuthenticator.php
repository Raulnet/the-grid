<?php

namespace App\Security;

use App\Exception\GridException;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Security\Core\Authentication\Token\TokenInterface;
use Symfony\Component\Security\Core\Exception\AuthenticationException;
use Symfony\Component\Security\Core\User\UserInterface;
use Symfony\Component\Security\Core\User\UserProviderInterface;
use Symfony\Component\Security\Guard\AbstractGuardAuthenticator;

class ApiAuthenticator extends AbstractGuardAuthenticator
{
    private $apiTokenProvider;

    public function __construct(ApiTokenProvider $apiTokenProvider)
    {
        $this->apiTokenProvider = $apiTokenProvider;
    }

    public function supports(Request $request): bool
    {
        return !empty($request->headers->get('Authorization'));
    }

    public function getCredentials(Request $request)
    {
        $jwt = str_replace('Bearer ', '', $request->headers->get('Authorization'));
        if (empty($jwt)) {
            throw new AuthenticationException('Authorization required');
        }
        try {
            return $this->apiTokenProvider->decodeToken($jwt);
        } catch (\Exception $e) {
            throw new GridException($e->getMessage(), 401);
        }
    }

    public function getUser($credentials, UserProviderInterface $userProvider): UserInterface
    {
        return $userProvider->loadUserByUsername($credentials->sub);
    }

    public function checkCredentials($credentials, UserInterface $user): bool
    {
        return true;
    }

    public function onAuthenticationFailure(Request $request, AuthenticationException $exception): JsonResponse
    {
        $data = [
            'message' => strtr($exception->getMessageKey(), $exception->getMessageData()),
        ];

        return new JsonResponse($data, JsonResponse::HTTP_FORBIDDEN);
    }

    public function onAuthenticationSuccess(Request $request, TokenInterface $token, $providerKey): void
    {
    }

    public function start(Request $request, AuthenticationException $authException = null): JsonResponse
    {
        $data = [
            'message' => 'Authentication Required',
        ];

        return new JsonResponse($data, JsonResponse::HTTP_UNAUTHORIZED);
    }

    public function supportsRememberMe(): bool
    {
        return false;
    }
}
