<?php

namespace App\Security\Model;

use Symfony\Component\Validator\Constraints as Assert;

class ApiAuthenticate
{
    /**
     * @Assert\Email
     */
    private $email;
    /**
     * @Assert\NotBlank
     */
    private $password;
    /**
     * @Assert\Uuid
     * @Assert\NotBlank
     */
    private $apiKey;

    public function getEmail()
    {
        return $this->email;
    }

    public function setEmail(string $email): void
    {
        $this->email = $email;
    }

    public function getPassword()
    {
        return $this->password;
    }

    public function setPassword(string $password): void
    {
        $this->password = $password;
    }

    public function getApiKey()
    {
        return $this->apiKey;
    }

    public function setApiKey(string $apiKey): void
    {
        $this->apiKey = $apiKey;
    }
}
