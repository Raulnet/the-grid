<?php

namespace App\Controller;

use App\Entity\User;
use App\Repository\UserRepository;
use App\Security\ApiTokenProvider;
use App\Security\Model\ApiAuthenticate;
use Doctrine\ORM\EntityManagerInterface;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;
use Symfony\Component\Security\Http\Authentication\AuthenticationUtils;
use Swagger\Annotations as SWG;

class SecurityController extends AbstractController
{
    /**
     * @Route("/login", name="app_login")
     */
    public function login(AuthenticationUtils $authenticationUtils): Response
    {
        // get the login error if there is one
        $error = $authenticationUtils->getLastAuthenticationError();
        // last username entered by the user
        $lastUsername = $authenticationUtils->getLastUsername();

        return $this->render('security/login.html.twig', ['last_username' => $lastUsername, 'error' => $error]);
    }

    /**
     * @Route("api/login", name="api_login", methods={"POST"})
     * @SWG\Response(response=201, description="get a token for succesfull credential")
     * @ParamConverter(name="apiAuthenticate", class="App\Security\Model\ApiAuthenticate", converter="body.converter")
     * @SWG\Parameter(
     *     name="api.login.json",
     *     in="body",
     *     type="json",
     *     schema={
     *         "type": "object",
     *         "properties": {
     *             "email": {"type": "string"},
     *             "password": {"type": "string"},
     *             "api_key": {"type": "string"},
     *         }
     *     }
     * )
     * @SWG\Tag(name="Security")
     */
    public function apiLogin(
        UserRepository $userRepository,
        EntityManagerInterface $entityManager,
        UserPasswordEncoderInterface $userPasswordEncoder,
        ApiTokenProvider $apiTokenProvider,
        ApiAuthenticate $apiAuthenticate
    ): JsonResponse {
        /** @var User $user */
        $user = $userRepository->findOneBy([
            'email' => $apiAuthenticate->getEmail(),
            'apiKey' => $apiAuthenticate->getApiKey(),
            'dateDeleted' => null,
        ]);
        if (!$user) {
            return $this->invalidCredentials();
        }
        if (!$userPasswordEncoder->isPasswordValid($user, $apiAuthenticate->getPassword())) {
            return $this->invalidCredentials();
        }
        $authToken = $apiTokenProvider->getAuthToken($user);
        $entityManager->persist($authToken);
        $entityManager->flush();

        return new JsonResponse(['token' => $apiTokenProvider->getToken($authToken)], 201);
    }

    private function invalidCredentials(): JsonResponse
    {
        return new JsonResponse(
            ['message' => 'Invalid credentials'],
            Response::HTTP_BAD_REQUEST
        );
    }
}
