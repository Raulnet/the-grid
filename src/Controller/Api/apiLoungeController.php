<?php

namespace App\Controller\Api;

use App\Entity\Lounge;
use App\Entity\Stage;
use App\Service\ObjectHydrator;
use Doctrine\ORM\EntityManagerInterface;
use Nelmio\ApiDocBundle\Annotation\Model;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Entity;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use Swagger\Annotations as SWG;

/**
 * @Route("/api/stages/{id}/lounges")
 */
class apiLoungeController extends AbstractApiController
{
    /**
     * @Route(name="api_lounges", methods={"GET"})
     * @Entity(name="stage", class="App\Entity\Stage", expr="repository.findOneBy({id: id, dateDeleted: null})")
     * @Entity(name="lounges", class="App\Entity\Lounge", expr="repository.findBy({stage: stage, dateDeleted: null})")
     * @SWG\Tag(name="Lounges")
     * @SWG\Response(response=200, description="get All Lounges by Stage")
     */
    public function lounges(array $lounges): JsonResponse
    {
        return $this->apiJsonResponse(200, $lounges);
    }

    /**
     * @Route("/{loungeId}", name="api_lounge", methods={"GET"})
     * @Entity(name="stage", class="App\Entity\Stage", expr="repository.findOneBy({id: id, dateDeleted: null})")
     * @Entity(name="lounge", class="App\Entity\Lounge", expr="repository.findOneBy({id: loungeId, stage: stage, dateDeleted: null})")
     * @SWG\Tag(name="Lounges")
     * @SWG\Response(response=200, description="get one lounge of Stage by id", @Model(type=Lounge::class))
     */
    public function lounge(Lounge $lounge): JsonResponse
    {
        return $this->apiJsonResponse(200, $lounge);
    }

    /**
     * @Route(name="api_create_lounges", methods={"POST"})
     * @Entity(name="stage", class="App\Entity\Stage", expr="repository.findOneBy({id: id, dateDeleted: null})")
     * @SWG\Tag(name="Lounges")
     * @ParamConverter(name="lounge", converter="body.converter", class="App\Entity\Lounge")
     *
     * @SWG\Response(response=201, description="create one lounge for one stage")
     * @SWG\Parameter(
     *     name="club.json",
     *     in="body",
     *     type="json",
     *     schema={
     *         "type": "object",
     *         "properties": {
     *             "title": {"type": "string"},
     *             "description": {"type": "string"}
     *         }
     *     }
     * )
     */
    public function createLounge(EntityManagerInterface $manager, Stage $stage, Lounge $lounge): JsonResponse
    {
        $manager->persist($lounge);
        $stage->addLounge($lounge);
        $manager->flush();

        return $this->apiJsonResponse(201);
    }

    /**
     * @Route("/{loungeId}", name="api_edit_lounges", methods={"PUT", "PATCH"})
     * @Entity(name="stage", class="App\Entity\Stage", expr="repository.findOneBy({id: id, dateDeleted: null})")
     * @Entity(name="lounge", class="App\Entity\Lounge", expr="repository.findOneBy({id: loungeId, stage: stage, dateDeleted: null})")
     * @SWG\Tag(name="Lounges")
     *
     * @SWG\Response(response=204, description="edit one lounge for one stage")
     * @SWG\Parameter(
     *     name="club.json",
     *     in="body",
     *     type="json",
     *     schema={
     *         "type": "object",
     *         "properties": {
     *             "title": {"type": "string"},
     *             "description": {"type": "string"}
     *         }
     *     }
     * )
     */
    public function editLounge(
        Request $request,
        ObjectHydrator $hydrator,
        EntityManagerInterface $manager,
        Lounge $lounge
    ): JsonResponse {
        $hydrator->hydrate($lounge, $request->getContent());
        $manager->flush();

        return $this->apiJsonResponse(204);
    }

    /**
     * @Route("/{loungeId}", name="api_remove_lounges", methods={"DELETE"})
     * @Entity(name="stage", class="App\Entity\Stage", expr="repository.findOneBy({id: id, dateDeleted: null})")
     * @Entity(name="lounge", class="App\Entity\Lounge", expr="repository.findOneBy({id: loungeId, stage: stage, dateDeleted: null})")
     * @SWG\Tag(name="Lounges")
     *
     * @SWG\Response(response=204, description="remove one lounge by id of one stage")
     */
    public function removeLounge(
        EntityManagerInterface $manager,
        Stage $stage,
        Lounge $lounge): JsonResponse
    {
        $stage->removeLounge($lounge);

        $manager->flush();

        return $this->apiJsonResponse(204);
    }
}
