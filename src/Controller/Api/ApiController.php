<?php

namespace App\Controller\Api;

use App\Repository\AuthTokenRepository;
use App\Security\ApiTokenProvider;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\Routing\Annotation\Route;
use Swagger\Annotations as SWG;

/**
 * @Route("/api")
 */
class ApiController extends AbstractApiController
{
    /**
     * @Route("/refresh", name="api_refresh_token", methods={"POST"})
     * @SWG\Tag(name="Security")
     * @SWG\Response(response=201, description="get new token before expiration time")
     */
    public function refreshToken(
        ApiTokenProvider $apiTokenProvider,
        AuthTokenRepository $authTokenRepository,
        EntityManagerInterface $entityManager
    ): JsonResponse {
        $authTokenRepository->deleteAuthToken($this->getUser());
        $authToken = $apiTokenProvider->getAuthToken($this->getUser());
        $entityManager->persist($authToken);
        $entityManager->flush();

        return $this->apiJsonResponse(201, ['token' => $apiTokenProvider->getToken($authToken)]);
    }
}
