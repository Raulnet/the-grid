<?php
/**
 * Created by PhpStorm.
 * User: raulnet
 * Date: 07/01/19
 * Time: 02:19
 */

namespace App\Controller\Api;

use App\Entity\ChampionshipRule;
use App\Repository\ChampionshipRuleRepository;
use App\Service\ObjectHydrator;
use Doctrine\ORM\EntityManagerInterface;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Entity;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;
use Swagger\Annotations as SWG;

/**
 * @Route("/api/championship-rules")
 */
class ApiChampionshipRuleController extends AbstractApiController
{
    /**
     * @Route(name="api_championship_rules", methods={"GET"})
     *
     * @SWG\Tag(name="Championship-rules")
     * @SWG\Response(response=200, description="get All Championship rules")
     */
    public function championshipRules(ChampionshipRuleRepository $championshipRuleRepository): JsonResponse
    {
        return $this->apiJsonResponse(200, $championshipRuleRepository->findBy([
            'dateDeleted' => null,
        ]));
    }

    /**
     * @Route("/{id}", name="api_championship_rule", methods={"GET"})
     * @Entity("championshipRule", expr="repository.findOneBy({id: id, dateDeleted: null})")
     *
     * @SWG\Tag(name="Championship-rules")
     * @SWG\Parameter(name="id", in="path", type="integer", description="championship-rule id")
     * @SWG\Response(response=200, description="get one Championship rule by id")
     */
    public function championshipRule(ChampionshipRule $championshipRule): JsonResponse
    {
        return $this->apiJsonResponse(200, $championshipRule);
    }

    /**
     * @Route(name="api_create_championship_rules", methods={"POST"})
     * @ParamConverter(name="championshipRule", converter="body.converter", class="App\Entity\ChampionshipRule")
     *
     * @SWG\Tag(name="Championship-rules")
     * @SWG\Response(response=201, description="create one Championship-rule")
     * @SWG\Parameter(
     *     name="competitor.json",
     *     in="body",
     *     type="json",
     *     schema={
     *         "type": "object",
     *         "properties": {
     *             "title": {"type": "string"},
     *             "description": {"type": "string"},
     *             "rules": {"type": "string"}
     *         }
     *     }
     * )
     */
    public function createChampionshipRules(
        ChampionshipRule $championshipRule,
        EntityManagerInterface $manager
    ): JsonResponse {
        $manager->persist($championshipRule);
        $manager->flush();

        return $this->apiJsonResponse(201);
    }

    /**
     * @Route("/{id}", name="api_edit_championship_rules", methods={"PATCH", "PUT"})
     * @Entity("championshipRule", expr="repository.findOneBy({id: id, dateDeleted: null})")
     *
     * @SWG\Tag(name="Championship-rules")
     * @SWG\Parameter(name="id", in="path", type="integer", description="championship-rule id")
     * @SWG\Response(response=204, description="edit one Championship-rule")
     * @SWG\Parameter(
     *     name="competitor.json",
     *     in="body",
     *     type="json",
     *     schema={
     *         "type": "object",
     *         "properties": {
     *             "title": {"type": "string"},
     *             "description": {"type": "string"},
     *             "rules": {"type": "string"}
     *         }
     *     }
     * )
     */
    public function editChampionshipRules(
        Request $request,
        ChampionshipRule $championshipRule,
        EntityManagerInterface $manager,
        ObjectHydrator $hydrator
    ): JsonResponse {
        $hydrator->hydrate($championshipRule, $request->getContent());
        $manager->persist($championshipRule);
        $manager->flush();

        return $this->apiJsonResponse(204);
    }

    /**
     * @Route("/{id}", name="api_delete_championship_rules", methods={"DELETE"})
     * @Entity("championshipRule", expr="repository.findOneBy({id: id, dateDeleted: null})")
     *
     * @SWG\Tag(name="Championship-rules")
     * @SWG\Parameter(name="id", in="path", type="integer", description="championship-rule id")
     * @SWG\Response(response=204, description="remove one Championship rule by id")
     */
    public function deleteChampionshipRules(
        ChampionshipRule $championshipRule,
        EntityManagerInterface $manager
    ): JsonResponse {
        $championshipRule->setDateDeleted(new \DateTime('now'));
        foreach ($championshipRule->getChampionships() as $championship) {
            $championshipRule->removeChampionship($championship);
        }
        $manager->flush();

        return $this->apiJsonResponse(204);
    }
}
