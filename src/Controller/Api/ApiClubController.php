<?php

namespace App\Controller\Api;

use App\Entity\Club;
use App\Entity\Competitor;
use App\Repository\ClubRepository;
use App\Service\ClubService;
use App\Service\ObjectHydrator;
use Doctrine\ORM\EntityManagerInterface;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Entity;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Swagger\Annotations as SWG;
use Nelmio\ApiDocBundle\Annotation\Model;

/**
 * @Route("/api/clubs")
 */
class ApiClubController extends AbstractApiController
{
    /**
     * @Route(name="api_clubs", methods={"GET"})
     *
     * @SWG\Tag(name="Clubs")
     * @SWG\Response(response=200, description="get All Clubs",
     *     @SWG\Schema(type="array", @SWG\Items(ref=@Model(type=Club::class)))
     * )
     */
    public function clubs(ClubRepository $repository): JsonResponse
    {
        return $this->apiJsonResponse(200, $repository->findBy(['dateDeleted' => null]));
    }

    /**
     * @Route("/{id}", name="api_club", methods={"GET"})
     * @Entity(name="club", expr="repository.findOneBy({id: id, dateDeleted: null})")
     *
     * @SWG\Tag(name="Clubs")
     * @SWG\Parameter(name="id", in="path", type="integer", description="club id")
     * @SWG\Response(response=200, description="get one Club by id", @Model(type=Club::class))
     */
    public function club(Club $club): JsonResponse
    {
        return $this->apiJsonResponse(200, $club);
    }

    /**
     * @Route(name="api_create_club", methods={"POST"})
     * @ParamConverter(name="club", converter="body.converter", class="App\Entity\Club")
     *
     * @SWG\Tag(name="Clubs")
     * @SWG\Response(response=201, description="create one clubs")
     * @SWG\Parameter(
     *     name="club.json",
     *     in="body",
     *     type="json",
     *     schema={
     *         "type": "object",
     *         "properties": {
     *             "title": {"type": "string"},
     *             "description": {"type": "string"}
     *         }
     *     }
     * )
     */
    public function createClub(Club $club, EntityManagerInterface $manager): JsonResponse
    {
        $manager->persist($club);
        $manager->flush();

        return $this->apiJsonResponse(201);
    }

    /**
     * @Route("/{id}", name="api_edit_club", methods={"PATCH", "PUT"})
     * @Entity(name="club", expr="repository.findOneBy({id: id, dateDeleted: null})")
     *
     * @SWG\Tag(name="Clubs")
     * @SWG\Parameter(name="id", in="path", type="integer", description="club id")
     * @SWG\Response(response=204, description="edit one clubs by id")
     * @SWG\Parameter(
     *     name="club.json",
     *     in="body",
     *     type="json",
     *     schema={
     *         "type": "object",
     *         "properties": {
     *             "title": {"type": "string"},
     *             "description": {"type": "string"}
     *         }
     *     }
     * )
     */
    public function editClub(
        Request $request,
        Club $club,
        EntityManagerInterface $manager,
        ObjectHydrator $hydrator
    ): JsonResponse {
        $hydrator->hydrate($club, $request->getContent());
        $manager->flush();

        return $this->apiJsonResponse(204);
    }

    /**
     * @Route("/{id}", name="api_remove_club", methods={"DELETE"})
     * @Entity(name="club", expr="repository.findOneBy({id: id, dateDeleted: null})")
     *
     * @SWG\Tag(name="Clubs")
     * @SWG\Parameter(name="id", in="path", type="integer", description="club id")
     * @SWG\Response(response=204, description="remove one clubs by id")
     */
    public function removeClub(
        Club $club,
        ClubService $service
    ): JsonResponse {
        $service->removeClub($club);

        return $this->apiJsonResponse(204);
    }

    /**
     * @Route("/{id}/competitors/{competitorId}", name="api_add_competitor_club", methods={"PATCH", "PUT"})
     * @Entity(name="club", expr="repository.findOneBy({id: id, dateDeleted: null})")
     * @Entity(name="competitor", expr="repository.findOneBy({id: competitorId, dateDeleted: null, club: null})")
     *
     * @SWG\Tag(name="Clubs")
     * @SWG\Parameter(name="id", in="path", type="integer", description="club id")
     * @SWG\Parameter(name="competitorId", in="path", type="integer", description="competitor id")
     * @SWG\Response(response=204, description="add competitor into club")
     */
    public function addCompetitorClub(
        Club $club,
        Competitor $competitor,
        EntityManagerInterface $manager
    ): JsonResponse {
        $club->addCompetitor($competitor);
        $manager->flush();

        return $this->apiJsonResponse(204);
    }

    /**
     * @Route("/{id}/competitors/{competitorId}", name="api_remove_competitor_club", methods={"DELETE"})
     * @Entity(name="club", expr="repository.findOneBy({id: id, dateDeleted: null})")
     * @Entity(name="competitor", expr="repository.findOneBy({id: competitorId, dateDeleted: null, club: club})")
     *
     * @SWG\Tag(name="Clubs")
     * @SWG\Parameter(name="id", in="path", type="integer", description="club id")
     * @SWG\Parameter(name="competitorId", in="path", type="integer", description="competitor id")
     * @SWG\Response(response=204, description="remove one competitor of club")
     */
    public function removeCompetitorClub(
        Club $club,
        Competitor $competitor,
        EntityManagerInterface $manager
    ): JsonResponse {
        $club->removeCompetitor($competitor);
        $manager->flush();

        return $this->apiJsonResponse(204);
    }
}
