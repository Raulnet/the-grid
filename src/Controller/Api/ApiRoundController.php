<?php

namespace App\Controller\Api;

use App\Entity\Round;
use App\Entity\Stage;
use App\Repository\RoundRepository;
use App\Service\ObjectHydrator;
use App\Service\RoundService;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Entity;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use Swagger\Annotations as SWG;

/**
 * @Route("/api/stages/{id}/rounds")
 */
class ApiRoundController extends AbstractApiController
{
    /**
     * @Route(name="api_rounds",
     *     methods={"GET"},
     *     requirements={
     *         "id": "\d+"
     *     }
     * )
     * @Entity(name="stage", expr="repository.findOneBy({id: id, dateDeleted: null})")
     * @SWG\Tag(name="Rounds")
     * @SWG\Response(response=200, description="get All Rounds by Stage")
     */
    public function rounds(Stage $stage, RoundRepository $repository): JsonResponse
    {
        return $this->apiJsonResponse(200, $repository->findBy([
            'stage' => $stage,
            'dateDeleted' => null,
        ]));
    }

    /**
     * @Route("/{roundId}",
     *     name="api_round",
     *     methods={"GET"},
     *     requirements={
     *         "id": "\d+",
     *         "roundId": "\d+"
     *     }
     * )
     * @Entity(name="stage", class="App\Entity\Stage", expr="repository.findOneBy({id: id, dateDeleted: null})")
     * @Entity(name="round", expr="repository.findOneBy({id: roundId, stage: stage, dateDeleted: null})")
     * @SWG\Tag(name="Rounds")
     * @SWG\Response(response=200, description="get one Round of a Stage")
     */
    public function round(Round $round): JsonResponse
    {
        return $this->apiJsonResponse(200, $round);
    }

    /**
     * @Route("/{roundId}/launch",
     *     name="api_round_launch",
     *     methods={"POST"},
     *     requirements={
     *         "id": "\d+",
     *         "roundId": "\d+"
     *     }
     * )
     * @Entity(name="stage", class="App\Entity\Stage", expr="repository.findOneBy({id: id, dateDeleted: null})")
     * @Entity(name="round", expr="repository.findOneBy({id: roundId, stage: stage, dateDeleted: null, currentPlace: 'build'})")
     * @SWG\Tag(name="Rounds")
     * @SWG\Response(response=201, description="launch one Round of a Stage")
     */
    public function roundLaunch(RoundService $roundService, Round $round): JsonResponse
    {
        $roundService->launch($round);

        return $this->apiJsonResponse(201);
    }

    /**
     * @Route(name="api_create_rounds",
     *     methods={"POST"},
     *     requirements={
     *         "id": "\d+"
     *     }
     * )
     * @ParamConverter(name="round", converter="body.converter", class="App\Entity\Round")
     * @SWG\Tag(name="Rounds")
     * @SWG\Response(response=201, description="create one rounds of stage")
     * @SWG\Parameter(
     *     name="round.json",
     *     in="body",
     *     type="json",
     *     schema={
     *         "type": "object",
     *         "properties": {
     *             "title": {"type": "string"},
     *             "description": {"type": "string"}
     *         }
     *     }
     * )
     */
    public function createRounds(Stage $stage, Round $round, RoundService $roundService): JsonResponse
    {
        $roundService->createRound($stage, $round);

        return $this->apiJsonResponse(201);
    }

    /**
     * @Route("/{roundId}",
     *     name="api_edit_rounds",
     *     methods={"PATCH", "PUT"},
     *     requirements={
     *         "id": "\d+",
     *         "roundId": "\d+"
     *     }
     * )
     * @Entity(name="stage", class="App\Entity\Stage", expr="repository.findOneBy({id: id, dateDeleted: null})")
     * @Entity(name="round", expr="repository.findOneBy({id: roundId, stage: stage, dateDeleted: null})")
     * @SWG\Tag(name="Rounds")
     * @SWG\Response(response=204, description="edit one rounds of stage")
     * @SWG\Parameter(
     *     name="round.json",
     *     in="body",
     *     type="json",
     *     schema={
     *         "type": "object",
     *         "properties": {
     *             "title": {"type": "string"},
     *             "description": {"type": "string"}
     *         }
     *     }
     * )
     */
    public function editRounds(
        Request $request,
        Round $round,
        RoundService $roundService,
        ObjectHydrator $hydrator
    ): JsonResponse {
        $hydrator->hydrate($round, $request->getContent());
        $roundService->editRound($round);

        return $this->apiJsonResponse(204);
    }

    /**
     * @Route("/{roundId}",
     *     name="api_remove_rounds",
     *     methods={"DELETE"},
     *     requirements={
     *         "id": "\d+",
     *         "roundId": "\d+"
     *     }
     * )
     * @Entity(name="stage", class="App\Entity\Stage", expr="repository.findOneBy({id: id, dateDeleted: null})")
     * @Entity(name="round", expr="repository.findOneBy({id: roundId, stage: stage, dateDeleted: null})")
     * @SWG\Tag(name="Rounds")
     * @SWG\Response(response=204, description="remove one Round of a Stage")
     */
    public function removeRounds(
        Round $round,
        RoundService $roundService
    ): JsonResponse {
        $roundService->removeRound($round);

        return $this->apiJsonResponse(204);
    }

    /**
     * @Route("/{roundId}/sequence/{sequence}",
     *     name="api_edit_sequnce_rounds",
     *     methods={"PATCH", "PUT"},
     *     requirements={
     *         "id": "\d+",
     *         "roundId": "\d+",
     *         "sequence": "\d+"
     *     }
     * )
     * @Entity(name="stage", class="App\Entity\Stage", expr="repository.findOneBy({id: id, dateDeleted: null})")
     * @Entity(name="round", expr="repository.findOneBy({id: roundId, stage: stage, dateDeleted: null})")
     * @SWG\Tag(name="Rounds")
     * @SWG\Response(response=204, description="change sequence of round")
     */
    public function editSequenceRounds(
        Round $round,
        RoundService $roundService,
        int $sequence
    ): JsonResponse {
        $roundService->editSequence($round, $sequence);

        return $this->apiJsonResponse(204);
    }
}
