<?php
/**
 * Created by PhpStorm.
 * User: raulnet
 * Date: 07/01/19
 * Time: 22:19
 */

namespace App\Controller\Api;

use App\Entity\Championship;
use App\Entity\ChampionshipCompetitor;
use App\Entity\Competitor;
use App\Repository\ChampionshipCompetitorRepository;
use App\Service\ChampionshipCompetitorService;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Entity;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\Routing\Annotation\Route;
use Swagger\Annotations as SWG;
use Nelmio\ApiDocBundle\Annotation\Security;

/**
 * @Route("/api/championships/{id}/competitors")
 */
class ApiChampionshipCompetitorController extends AbstractApiController
{
    /**
     * @Route(methods={"GET"}, name="api_championship_competitors")
     * @Entity("championship", expr="repository.findOneBy({id: id, dateDeleted: null})")
     * @SWG\Tag(name="Championships")
     * @SWG\Response(response=200, description="get All Championshipss")
     * @Security(name="Bearer")
     */
    public function championshipCompetitors(
        ChampionshipCompetitorRepository $repository,
        Championship $championship
    ): JsonResponse {
        return $this->apiJsonResponse(200,
            $repository->findBy([
                'championship' => $championship,
                'dateDeleted' => null,
            ])
        );
    }

    /**
     * @Route("/{competitorId}", methods={"GET"}, name="api_championship_competitor")
     * @Entity("championship", class="App\Entity\Championship", expr="repository.findOneBy({id: id, dateDeleted: null})")
     * @Entity("competitor", class="App\Entity\Competitor", expr="repository.findOneBy({id: competitorId, dateDeleted: null})")
     * @Entity("championshipCompetitor", expr="repository.findOneBy({championship: championship, competitor: competitor, dateDeleted: null})")
     * @SWG\Tag(name="Championships")
     * @SWG\Response(response=200, description="get one Championships by Id")
     * @Security(name="Bearer")
     */
    public function championshipCompetitor(
        ChampionshipCompetitor $championshipCompetitor
    ): JsonResponse {
        return $this->apiJsonResponse(200, $championshipCompetitor);
    }

    /**
     * @Route("/{competitorId}", methods={"POST"}, name="api_create_championship_competitor")
     * @Entity("championship", expr="repository.findOneBy({id: id, dateDeleted: null})")
     * @Entity("competitor", expr="repository.findOneBy({id: competitorId, dateDeleted: null})")
     * @SWG\Tag(name="Championships")
     * @SWG\Response(response=201, description="get create Championships")
     * @Security(name="Bearer")
     */
    public function createChampionshipCompetitor(
        Championship $championship,
        Competitor $competitor,
        ChampionshipCompetitorService $service
    ): JsonResponse {
        $service->createChampionshipCompetitor($championship, $competitor);

        return $this->apiJsonResponse(201);
    }

    /**
     * @Route("/{competitorId}", methods={"DELETE"}, name="api_delete_championship_competitor")
     * @Entity("championship", class="App\Entity\Championship", expr="repository.findOneBy({id: id, dateDeleted: null})")
     * @Entity("competitor", class="App\Entity\Competitor", expr="repository.findOneBy({id: competitorId, dateDeleted: null})")
     * @Entity("championshipCompetitor", expr="repository.findOneBy({championship: championship, competitor: competitor, dateDeleted: null})")
     * @SWG\Tag(name="Championships")
     * @SWG\Response(response=200, description="get one Championships by Id")
     * @Security(name="Bearer")
     */
    public function removeChampionshipCompetitor(
        Championship $championship,
        ChampionshipCompetitor $championshipCompetitor,
        ChampionshipCompetitorService $service
    ): JsonResponse {
        $service->removeChampionshipCompetitor($championship, $championshipCompetitor);

        return $this->apiJsonResponse(204);
    }
}
