<?php

namespace App\Controller\Api;

use App\Entity\Lounge;
use App\Entity\Competitor;
use App\Entity\LoungeCompetitor;
use App\Entity\StageCompetitor;
use App\Repository\LoungeCompetitorRepository;
use App\Service\LoungeCompetitorService;
use Doctrine\ORM\EntityManagerInterface;
use Nelmio\ApiDocBundle\Annotation\Model;
use Nelmio\ApiDocBundle\Annotation\Security;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Entity;
use Swagger\Annotations as SWG;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpKernel\Exception\HttpException;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/api/lounges/{id}/competitors")
 */
class ApiLoungeCompetitorController extends AbstractApiController
{
    /**
     * @Route(methods={"GET"}, name="api_lounge_competitors")
     * @Entity("lounge", expr="repository.findOneBy({id: id, dateDeleted: null})")
     *
     * @SWG\Tag(name="Lounges")
     * @SWG\Parameter(name="id", in="path", type="integer", description="lounge id")
     * @SWG\Response(
     *     response=200,
     *     description="get All Lounge-competitors by lounge",
     *     @SWG\Items(
     *         ref=@Model(type=LoungeCompetitor::class)
     *     )
     * )
     * @Security(name="Bearer")
     */
    public function loungeCompetitors(Lounge $lounge, LoungeCompetitorRepository $repository): JsonResponse
    {
        return $this->apiJsonResponse(200, $repository->findBy([
            'lounge' => $lounge,
            'dateDeleted' => null,
        ]));
    }

    /**
     * @Route("/{competitorId}", methods={"GET"}, name="api_lounge_competitor")
     * @Entity("lounge", class="App\Entity\Lounge", expr="repository.findOneBy({id: id, dateDeleted: null})")
     * @Entity("competitor", class="App\Entity\Competitor", expr="repository.findOneBy({id: competitorId, dateDeleted: null})")
     * @Entity("loungeCompetitor", expr="repository.findOneBy({lounge: lounge, competitor: competitor, dateDeleted: null})")
     *
     * @SWG\Tag(name="Lounges")
     * @SWG\Parameter(name="id", in="path", type="integer", description="lounge id")
     * @SWG\Parameter(name="competitorId", in="path", type="integer", description="competitorId")
     * @SWG\Response(
     *     response=200,
     *     description="get one Lounge-competitors by id of lounge",
     *     @Model(type=LoungeCompetitor::class)
     * )
     * @Security(name="Bearer")
     */
    public function loungeCompetitor(LoungeCompetitor $loungeCompetitor): JsonResponse
    {
        return $this->apiJsonResponse(200, $loungeCompetitor);
    }

    /**
     * @Route("/{competitorId}", name="api_create_lounge_competitor", methods={"POST"})
     * @Entity("lounge", class="App\Entity\Lounge", expr="repository.findOneBy({id: id, dateDeleted: null})")
     * @Entity("competitor", class="App\Entity\Competitor", expr="repository.findOneBy({id: competitorId, dateDeleted: null})")
     * @Entity("stageCompetitor", expr="repository.findStageCompetitorByLounge(lounge, competitor)")
     *
     * @SWG\Tag(name="Lounges")
     * @SWG\Parameter(name="id", in="path", type="integer", description="lounge id")
     * @SWG\Parameter(name="competitorId", in="path", type="integer", description="competitorId")
     * @SWG\Response(
     *     response=201,
     *     description="create one Lounge-competitors by idCompetitor into a lounge",
     * )
     * @Security(name="Bearer")
     */
    public function createLoungeCompetitor(
        LoungeCompetitorRepository $repository,
        Lounge $lounge,
        Competitor $competitor,
        StageCompetitor $stageCompetitor,
        LoungeCompetitorService $service
    ): JsonResponse {
        if ($repository->findOneBy(['dateDeleted' => null, 'lounge' => $lounge, 'competitor' => $competitor])) {
            throw new HttpException(409, 'http.exception.lounge.already.contains.competitor');
        }
        $service->createLoungeCompetitor($lounge, $stageCompetitor);

        return $this->apiJsonResponse(201);
    }

    /**
     * @Route("/{competitorId}", name="api_remove_lounge_competitor", methods={"DELETE"})
     * @Entity("lounge", class="App\Entity\Lounge", expr="repository.findOneBy({id: id, dateDeleted: null})")
     * @Entity("competitor", class="App\Entity\Competitor", expr="repository.findOneBy({id: competitorId, dateDeleted: null})")
     * @Entity("loungeCompetitor", expr="repository.findOneBy({lounge: lounge, competitor: competitor, dateDeleted: null})")
     *
     * @SWG\Tag(name="Lounges")
     * @SWG\Parameter(name="id", in="path", type="integer", description="lounge id")
     * @SWG\Parameter(name="competitorId", in="path", type="integer", description="competitorId")
     * @SWG\Response(
     *     response=204,
     *     description="remove Lounge-competitors from lounge by idCompetitor",
     * )
     * @Security(name="Bearer")
     */
    public function removeLoungeCompetitor(
        EntityManagerInterface $manager,
        LoungeCompetitor $loungeCompetitor
    ): JsonResponse {
        $loungeCompetitor->setDateDeleted(new \DateTime('now'));
        $manager->flush();

        return $this->apiJsonResponse(204);
    }
}
