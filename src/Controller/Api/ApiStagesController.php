<?php
/**
 * Created by PhpStorm.
 * User: raulnet
 * Date: 06/01/19
 * Time: 23:22
 */

namespace App\Controller\Api;

use App\Entity\Championship;
use App\Entity\Stage;
use App\Repository\StageRepository;
use App\Service\ObjectHydrator;
use Doctrine\ORM\EntityManagerInterface;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Entity;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use Swagger\Annotations as SWG;
use Nelmio\ApiDocBundle\Annotation\Security;

/**
 * @Route("/api")
 */
class ApiStagesController extends AbstractApiController
{
    /**
     * @Route("/championships/{id}/stages", name="api_stages", methods={"GET"}, requirements={"id": "\d+"})
     * @Entity("championship", expr="repository.findOneBy({id: id, dateDeleted: null})")
     * @SWG\Tag(name="Stages")
     * @SWG\Response(response=200, description="get all stages")
     * @Security(name="Bearer")
     */
    public function stages(Championship $championship, StageRepository $stageRepository): JsonResponse
    {
        return $this->apiJsonResponse(200, $stageRepository->findBy([
            'dateDeleted' => null,
            'championship' => $championship,
        ]));
    }

    /**
     * @Route("/stages/{id}", name="api_stage", methods={"GET"}, requirements={"id": "\d+"})
     * @Entity("stage", expr="repository.findOneBy({id: id, dateDeleted: null})")
     * @SWG\Tag(name="Stages")
     * @SWG\Response(response=200, description="get one stage by id")
     * @Security(name="Bearer")
     */
    public function stage(Stage $stage): JsonResponse
    {
        return $this->apiJsonResponse(200, $stage);
    }

    /**
     * @Route(
     *     "/championships/{id}/stages",
     *     name="api_create_stage",
     *     methods={"POST"},
     *     requirements={"id": "\d+"}
     * )
     * @Entity("championship", expr="repository.findOneBy({id: id, dateDeleted: null})")
     * @ParamConverter(
     *     name="stage",
     *     converter="body.converter",
     *     class="App\Entity\Stage"
     * )
     * @SWG\Tag(name="Stages")
     * @SWG\Response(response=201, description="create stage")
     * @Security(name="Bearer")
     */
    public function createStage(Championship $championship, Stage $stage, EntityManagerInterface $manager): JsonResponse
    {
        $championship->addStage($stage);
        $manager->persist($stage);
        $manager->flush();

        return $this->apiJsonResponse(201);
    }

    /**
     * @Route(
     *     "/championships/{id}/stages/{stageId}",
     *     name="api_edit_stage",
     *     methods={"PATCH", "PUT"},
     *     requirements={
     *         "id": "\d+",
     *         "stageId": "\d+"
     *     }
     * )
     * @Entity("championship", expr="repository.findOneBy({id: id, dateDeleted: null})")
     * @Entity("stage", expr="repository.findOneBy({id: stageId, championship: championship, dateDeleted: null})")
     * @SWG\Tag(name="Stages")
     * @SWG\Response(response=201, description="edit stage")
     * @Security(name="Bearer")
     */
    public function editStage(
        Request $request,
        Championship $championship,
        Stage $stage,
        EntityManagerInterface $manager,
        ObjectHydrator $hydrator
    ): JsonResponse {
        $hydrator->hydrate($stage, $request->getContent());
        $championship->addStage($stage);
        $manager->persist($stage);
        $manager->flush();

        return $this->apiJsonResponse(204);
    }

    /**
     * @Route(
     *     "/championships/{id}/stages/{stageId}",
     *     name="api_delete_stage",
     *     methods={"DELETE"},
     *     requirements={
     *         "id": "\d+",
     *         "stageId": "\d+"
     *     }
     * )
     * @Entity("championship", expr="repository.findOneBy({id: id, dateDeleted: null})")
     * @Entity("stage", expr="repository.findOneBy({id: stageId, championship: championship, dateDeleted: null})")
     * @SWG\Tag(name="Stages")
     * @SWG\Response(response=201, description="remove stage")
     * @Security(name="Bearer")
     */
    public function deleteStage(
        Championship $championship,
        Stage $stage,
        EntityManagerInterface $manager
    ): JsonResponse {
        $championship->removeStage($stage);
        $stage->setDateDeleted(new \DateTime('now'));
        $manager->persist($stage);
        $manager->flush();

        return $this->apiJsonResponse(204);
    }
}
