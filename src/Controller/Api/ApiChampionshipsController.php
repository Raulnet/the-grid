<?php
/**
 * Created by PhpStorm.
 * User: raulnet
 * Date: 04/01/19
 * Time: 22:31
 */

namespace App\Controller\Api;

use App\Entity\Championship;
use App\Entity\ChampionshipRule;
use App\Service\ObjectHydrator;
use Doctrine\ORM\EntityManagerInterface;
use Nelmio\ApiDocBundle\Annotation\Model;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Entity;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;
use Symfony\Component\HttpFoundation\JsonResponse;
use App\Repository\ChampionshipRepository;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use Swagger\Annotations as SWG;
use Nelmio\ApiDocBundle\Annotation\Security;

/**
 * @Route("/api/championships")
 */
class ApiChampionshipsController extends AbstractApiController
{
    /**
     * @Route(name="api_championships", methods={"GET"})
     * @SWG\Tag(name="Championships")
     * @SWG\Response(response=200, description="get All Championships",
     *     @SWG\Schema(type="array", @SWG\Items(ref=@Model(type=Championship::class)))
     * )
     */
    public function championships(ChampionshipRepository $championshipRepository): JsonResponse
    {
        return $this->apiJsonResponse(200, $championshipRepository->findBy([
            'dateDeleted' => null,
        ]));
    }

    /**
     * @Route("/{id}", name="api_championship", methods={"GET"},
     *     requirements={
     *         "id": "\d+"
     *     })
     *     @Entity("championship", expr="repository.findOneBy({id: id, dateDeleted: null})")
     *     @SWG\Tag(name="Championships")
     *     @SWG\Parameter(name="id", in="path", type="integer", description="championship id")
     *     @SWG\Response(
     *         response=200,
     *         description="get one Championship by id",
     *         @Model(type=Championship::class)
     *     )
     * )
     *     @Security(name="Bearer")
     */
    public function championship(Championship $championship): JsonResponse
    {
        return $this->apiJsonResponse(200, $championship);
    }

    /**
     * @Route(name="api_create_championship", methods={"POST"})
     * @ParamConverter(name="championship", converter="body.converter", class="App\Entity\Championship")
     * @SWG\Tag(name="Championships")
     * @SWG\Response(response=201, description="create Championship")
     * @Security(name="Bearer")
     */
    public function createChampionship(Championship $championship, EntityManagerInterface $manager): JsonResponse
    {
        $manager->persist($championship);
        $manager->flush();

        return $this->apiJsonResponse(201);
    }

    /**
     * @Route("/{id}", name="api_edit_championship", methods={"PATCH"},
     *     requirements={
     *         "id": "\d+"
     *     })
     *     @Entity("championship", expr="repository.findOneBy({id: id, dateDeleted: null})")
     *     @SWG\Tag(name="Championships")
     *     @SWG\Parameter(name="id", in="path", type="integer", description="championship id")
     *     @SWG\Response(response=204, description="edit Championship")
     *     @Security(name="Bearer")
     */
    public function editChampionship(
        Request $request,
        Championship $championship,
        EntityManagerInterface $manager,
        ObjectHydrator $hydrator
    ): JsonResponse {
        $hydrator->hydrate($championship, $request->getContent());
        $manager->persist($championship);
        $manager->flush();

        return $this->apiJsonResponse(204);
    }

    /**
     * @Route("/{id}/championship-rule/{championshipRuleId}",
     *     name="api_championship_add_rule",
     *     methods={"PATCH", "PUT"},
     *     requirements={
     *         "id": "\d+",
     *         "championshipRuleId": "\d+"
     *     }
     * )
     * @Entity("championship", expr="repository.findOneBy({id: id, dateDeleted: null})")
     * @Entity("championshipRule", expr="repository.findOneBy({id: championshipRuleId, dateDeleted: null})")
     * @SWG\Tag(name="Championships")
     * @SWG\Parameter(name="id", in="path", type="integer", description="championship id")
     * @SWG\Parameter(name="championshipRuleId", in="path", type="integer", description="championship rule id")
     * @SWG\Response(response=204, description="edit Championship add rule")
     * @Security(name="Bearer")
     */
    public function addChampionshipRule(
        Championship $championship,
        ChampionshipRule $championshipRule,
        EntityManagerInterface $manager
    ): JsonResponse {
        $championshipRule->addChampionship($championship);
        $manager->flush();

        return $this->apiJsonResponse(204);
    }

    /**
     * @Route("/{id}/championship-rule/{championshipRuleId}",
     *     name="api_championship_remove_rule",
     *     methods={"DELETE"},
     *     requirements={
     *         "id": "\d+",
     *         "championshipRuleId": "\d+"
     *     }
     * )
     * @Entity("championship", expr="repository.findOneBy({id: id, dateDeleted: null})")
     * @Entity("championshipRule", expr="repository.findOneBy({id: championshipRuleId, dateDeleted: null})")
     * @SWG\Tag(name="Championships")
     * @SWG\Parameter(name="id", in="path", type="integer", description="championship id")
     * @SWG\Parameter(name="championshipRuleId", in="path", type="integer", description="championship rule id")
     * @SWG\Response(response=204, description="edit Championship remove rule")
     * @Security(name="Bearer")
     */
    public function removeChampionshipRule(
        Championship $championship,
        ChampionshipRule $championshipRule,
        EntityManagerInterface $manager
    ): JsonResponse {
        $championshipRule->removeChampionship($championship);
        $manager->flush();

        return $this->apiJsonResponse(204);
    }
}
