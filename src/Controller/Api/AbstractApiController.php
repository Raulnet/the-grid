<?php
/**
 * Created by PhpStorm.
 * User: raulnet
 * Date: 01/01/19
 * Time: 22:25
 */

namespace App\Controller\Api;

use App\Service\NormalizerService;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;

abstract class AbstractApiController extends AbstractController
{
    private $normalizerService;

    public function __construct(NormalizerService $normalizerService)
    {
        $this->normalizerService = $normalizerService;
    }

    protected function apiJsonResponse(int $statusCode = 200, $data = null): JsonResponse
    {
        $response = new JsonResponse();
        $response->setStatusCode($statusCode);
        if (!empty($data)) {
            $response->setData($this->normalizerService->normalize($data));
        }

        return $response;
    }
}
