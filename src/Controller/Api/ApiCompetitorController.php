<?php
/**
 * Created by PhpStorm.
 * User: raulnet
 * Date: 07/01/19
 * Time: 22:20
 */

namespace App\Controller\Api;

use App\Entity\Club;
use App\Entity\Competitor;
use App\Repository\CompetitorRepository;
use App\Service\ObjectHydrator;
use Doctrine\ORM\EntityManagerInterface;
use Nelmio\ApiDocBundle\Annotation\Model;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Entity;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Swagger\Annotations as SWG;

/** @Route("/api/competitors") */
class ApiCompetitorController extends AbstractApiController
{
    /**
     * @Route(name="api_competitors", methods={"GET"})
     *
     * @SWG\Tag(name="Competitors")
     * @SWG\Response(response=200, description="get All Competitors",
     *     @SWG\Schema(type="array", @SWG\Items(ref=@Model(type=Competitor::class)))
     * )
     */
    public function competitors(CompetitorRepository $competitorRepository): JsonResponse
    {
        return $this->apiJsonResponse(200, $competitorRepository->findBy(['dateDeleted' => null]));
    }

    /**
     * @Route("/{id}", name="api_competitor", methods={"GET"})
     * @Entity("competitor", expr="repository.findOneBy({id: id, dateDeleted: null})")
     *
     * @SWG\Tag(name="Competitors")
     * @SWG\Parameter(name="id", in="path", type="integer", description="competitor id")
     * @SWG\Response(response=200, description="get one Competitor by id")
     */
    public function competitor(Competitor $competitor): JsonResponse
    {
        return $this->apiJsonResponse(200, $competitor);
    }

    /**
     * @Route(name="api_create_competitors", methods={"POST"})
     * @ParamConverter(name="competitor", converter="body.converter", class="App\Entity\Competitor")
     *
     * @SWG\Tag(name="Competitors")
     * @SWG\Response(response=201, description="create one competitor")
     * @SWG\Parameter(
     *     name="competitor.json",
     *     in="body",
     *     type="json",
     *     schema={
     *         "type": "object",
     *         "properties": {
     *             "username": {"type": "string"},
     *             "country": {"type": "string"},
     *             "bid": {"type": "string"}
     *         }
     *     }
     * )
     */
    public function createCompetitor(Competitor $competitor, EntityManagerInterface $manager): JsonResponse
    {
        $manager->persist($competitor);
        $manager->flush();

        return $this->apiJsonResponse(201);
    }

    /**
     * @Route("/{id}", name="api_edit_competitor", methods={"PUT", "PATCH"})
     * @Entity("competitor", expr="repository.findOneBy({id: id, dateDeleted: null})")
     *
     * @SWG\Tag(name="Competitors")
     * @SWG\Parameter(name="id", in="path", type="integer", description="competitor id")
     * @SWG\Response(response=204, description="update one competitor by id")
     * @SWG\Parameter(
     *     name="competitor.json",
     *     in="body",
     *     type="json",
     *     schema={
     *         "type": "object",
     *         "properties": {
     *             "username": {"type": "string"},
     *             "country": {"type": "string"},
     *             "bid": {"type": "string"}
     *         }
     *     }
     * )
     */
    public function editCompetitor(
        Request $request,
        Competitor $competitor,
        EntityManagerInterface $manager,
        ObjectHydrator $hydrator
    ): JsonResponse {
        $hydrator->hydrate($competitor, $request->getContent());
        $manager->flush();

        return $this->apiJsonResponse(204);
    }

    /**
     * @Route("/{id}", name="api_delete_competitor", methods={"DELETE"})
     * @Entity("competitor", expr="repository.findOneBy({id: id, dateDeleted: null})")
     *
     * @SWG\Tag(name="Competitors")
     * @SWG\Parameter(name="id", in="path", type="integer", description="competitor id")
     * @SWG\Response(response=204, description="remove one competitor by id")
     */
    public function deleteCompetitor(
        Competitor $competitor,
        EntityManagerInterface $manager
    ): JsonResponse {
        $competitor->setDateDeleted(new \DateTime('now'));
        $manager->flush();

        return $this->apiJsonResponse(204);
    }

    /**
     * @Route("/{id}/clubs/{clubId}", name="api_add_club_competitor", methods={"PATCH", "PUT"})
     * @Entity("competitor", expr="repository.findOneBy({id: id, dateDeleted: null})")
     * @Entity("club", expr="repository.findOneBy({id: clubId, dateDeleted: null})")
     *
     * @SWG\Tag(name="Competitors")
     * @SWG\Parameter(name="id", in="path", type="integer", description="competitor id")
     * @SWG\Parameter(name="clubId", in="path", type="integer", description="club id")
     * @SWG\Response(response=204, description="add one competitor on club")
     */
    public function addClubCompetitor(
        Competitor $competitor,
        Club $club,
        EntityManagerInterface $manager
    ): JsonResponse {
        $club->addCompetitor($competitor);
        $manager->flush();

        return $this->apiJsonResponse(204);
    }

    /**
     * @Route("/{id}/clubs/{clubId}", name="api_remove_club_competitor", methods={"DELETE"})
     * @Entity("competitor", expr="repository.findOneBy({id: id, dateDeleted: null})")
     * @Entity("club", expr="repository.findOneBy({id: clubId, dateDeleted: null})")
     *
     * @SWG\Tag(name="Competitors")
     * @SWG\Parameter(name="id", in="path", type="integer", description="competitor id")
     * @SWG\Parameter(name="clubId", in="path", type="integer", description="club id")
     * @SWG\Response(response=204, description="remove one competitor on club")
     */
    public function removeClubCompetitor(
        Competitor $competitor,
        Club $club,
        EntityManagerInterface $manager
    ): JsonResponse {
        $club->removeCompetitor($competitor);
        $manager->flush();

        return $this->apiJsonResponse(204);
    }
}
