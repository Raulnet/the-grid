<?php

namespace App\Controller\Api;

use App\Entity\ChampionshipCompetitor;
use App\Entity\Competitor;
use App\Entity\Stage;
use App\Entity\StageCompetitor;
use App\Repository\StageCompetitorRepository;
use App\Service\StageCompetitorsService;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Entity;
use Symfony\Component\HttpKernel\Exception\HttpException;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\JsonResponse;
use Swagger\Annotations as SWG;

/**
 * @Route("/api/stages/{id}/competitors")
 */
class ApiStageCompetitorController extends AbstractApiController
{
    /**
     * @Route(name="api_stage_competitors", methods={"GET"})
     * @SWG\Response(response=200, description="get All Stage-competitors by stage")
     */
    public function stagesCompetitors(Stage $stage, StageCompetitorRepository $repository): JsonResponse
    {
        return $this->apiJsonResponse(200, $repository->findBy([
            'dateDeleted' => null,
            'stage' => $stage,
        ]));
    }

    /**
     * @Route("/{competitorId}", name="api_stage_competitor", methods={"GET"})
     * @Entity("stage", expr="repository.findOneBy({id: id, dateDeleted: null})")
     * @Entity("competitor", expr="repository.findOneBy({id: competitorId, dateDeleted: null})")
     * @Entity("stageCompetitor", expr="repository.findOneBy({stage: stage, competitor: competitor, dateDeleted: null})")
     * @SWG\Response(response=200, description="get All Stage-competitors by stage && competitor")
     */
    public function stagesCompetitor(
        Stage $stage,
        Competitor $competitor,
        StageCompetitor $stageCompetitor
    ): JsonResponse {
        return $this->apiJsonResponse(200, $stageCompetitor);
    }

    /**
     * @Route("/{competitorId}", name="api_create_stage_competitor", methods={"POST"})
     * @Entity("stage", expr="repository.findOneBy({id: id, dateDeleted: null})")
     * @Entity("competitor", expr="repository.findOneBy({id: competitorId, dateDeleted: null})")
     * @Entity("championshipCompetitor", expr="repository.findChampionshipCompetitorByStage(stage, competitor)")
     * @SWG\Response(response=200, description="get All Stage-competitors by stage && competitor")
     */
    public function createStageCompetitor(
        StageCompetitorRepository $repository,
        StageCompetitorsService $service,
        Stage $stage,
        Competitor $competitor,
        ChampionshipCompetitor $championshipCompetitor
    ): JsonResponse {
        if ($repository->findOneBy(['stage' => $stage, 'competitor' => $competitor, 'dateDeleted' => null])) {
            throw new HttpException(409, 'http.exception.stage.already.contains.competitor');
        }
        $service->createStageCompetitor($stage, $championshipCompetitor);

        return $this->apiJsonResponse(201);
    }
}
