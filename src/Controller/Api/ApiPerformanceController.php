<?php

namespace App\Controller\Api;

use App\Entity\Lounge;
use App\Entity\Round;
use App\Repository\PerformanceRepository;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Entity;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Swagger\Annotations as SWG;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;

/**
 * @Route("/api/lounges/{loungeId}")
 */
class ApiPerformanceController extends AbstractApiController
{
    /**
     * @Route("/rounds/{roundId}/performances", name="api_lounge_round_performances", methods={"GET"})
     * @Entity(name="lounge", class="App\Entity\Lounge", expr="repository.findOneBy({id: loungeId, dateDeleted: null})")
     * @Entity(name="round", class="App\Entity\Round", expr="repository.findOneBy({id: roundId, dateDeleted: null})")
     *
     * @SWG\Tag(name="Performances")
     * @SWG\Response(response=200, description="find all performances by lounge and round")
     */
    public function loungeRoundPerformances(PerformanceRepository $repository, Lounge $lounge, Round $round): JsonResponse
    {
        return $this->apiJsonResponse(200, $repository->findPerformances($lounge, $round));
    }

    /**
     * @Route("/performances", name="api_lounge_performances", methods={"GET"})
     * @Entity(name="lounge", class="App\Entity\Lounge", expr="repository.findOneBy({id: loungeId, dateDeleted: null})")
     *
     * @SWG\Tag(name="Performances")
     * @SWG\Response(response=200, description="find all performances by lounge")
     */
    public function loungePerformances(PerformanceRepository $repository, Lounge $lounge): JsonResponse
    {
        return $this->apiJsonResponse(200, $repository->findPerformances($lounge));
    }

    /**
     * @Route("/rounds/{roundId}/performances", name="api_create_performances", methods={"POST"})
     * @Entity(name="lounge", class="App\Entity\Lounge", expr="repository.findOneBy({id: loungeId, dateDeleted: null})")
     * @Entity(name="round", class="App\Entity\Round", expr="repository.findOneBy({id: roundId, dateDeleted: null})")
     * @ParamConverter(name="performances", converter="performances.converter", class="App\Entity\Performances[]")
     *
     * @SWG\Tag(name="Performances")
     * @SWG\Response(response=201, description="Create performances for lounge behind round")
     */
    public function createPerformance(Request $request, Lounge $lounge, Round $round, array $performances): JsonResponse
    {
        return $this->apiJsonResponse(201);
    }
}
