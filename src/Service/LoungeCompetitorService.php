<?php

namespace App\Service;

use App\Entity\Lounge;
use App\Entity\LoungeCompetitor;
use App\Entity\StageCompetitor;
use Doctrine\ORM\EntityManagerInterface;

class LoungeCompetitorService
{
    private $manager;

    public function __construct(EntityManagerInterface $manager)
    {
        $this->manager = $manager;
    }

    public function createLoungeCompetitor(Lounge $lounge, StageCompetitor $stageCompetitor): void
    {
        $loungeCompetitor = new LoungeCompetitor();
        $loungeCompetitor->setLounge($lounge);
        $loungeCompetitor->setStageCompetitor(
            $stageCompetitor
        );
        $this->manager->persist($loungeCompetitor);
        $this->manager->flush();
    }
}
