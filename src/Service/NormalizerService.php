<?php
/**
 * Created by PhpStorm.
 * User: raulnet
 * Date: 01/01/19
 * Time: 22:46
 */

namespace App\Service;

use App\Entity\Interfaces\EntityContentInterface;
use App\Entity\Interfaces\EntityIdentityInterface;
use Doctrine\Common\Collections\Collection;
use Symfony\Component\Serializer\Serializer;
use Symfony\Component\Serializer\Encoder\JsonEncoder;
use Symfony\Component\Serializer\Normalizer\GetSetMethodNormalizer;

class NormalizerService
{
    public function normalize($data)
    {
        $normalizer = new GetSetMethodNormalizer();
        $normalizer->setCircularReferenceHandler(function ($object) {
            if ($object instanceof EntityIdentityInterface) {
                return $object->getId();
            }

            return null;
        });
        $normalizer->setIgnoredAttributes(['sequenceCollection', 'className', 'salt', 'password', 'email']);
        $normalizer->setCallbacks($this->getCallBack());
        $serializer = new Serializer([$normalizer], [new JsonEncoder()]);

        return $serializer->normalize($data, 'json');
    }

    private function getCallBack(): array
    {
        $callBackCollection = $this->getCallBackCollections();
        $callbacks = array_merge($this->getCallBackProperties(), $this->getCallBackEntityProperties());

        return array_merge($callbacks, $callBackCollection);
    }

    private function getCallBackProperties(): array
    {
        return [
            'dateCreation' => $this->getCallBackDatetime(),
            'dateUpdate' => $this->getCallBackDatetime(),
            'dateDeleted' => $this->getCallBackDatetime(),
            'dateStart' => $this->getCallBackDatetime(),
            'dateEnd' => $this->getCallBackDatetime(),
        ];
    }

    private function getCallBackDatetime(): callable
    {
        return function ($innerObject) {
            return $innerObject instanceof \DateTime ? $innerObject->format(\DateTime::ATOM) : null;
        };
    }

    private function getCallBackEntityProperties(): array
    {
        return [
            'championshipRule' => $this->getCallBackEntity(),
            'championship' => $this->getCallBackEntity(),
            'stage' => $this->getCallBackEntity(),
            'round' => $this->getCallBackEntity(),
            'lounge' => $this->getCallBackEntity(),
        ];
    }

    private function getCallBackEntity(): callable
    {
        return function ($object) {
            $data = [];
            if ($object instanceof EntityIdentityInterface) {
                $data['id'] = $object->getId();
                $data['uuid'] = $object->getUuid();
            }

            if ($object instanceof EntityContentInterface) {
                $response['title'] = $object->getTitle();
            }

            return $data;
        };
    }

    private function getCallBackCollections(): array
    {
        return [
            'championships' => $this->getCallBackCollection(),
            'stages' => $this->getCallBackCollection(),
            'rounds' => $this->getCallBackCollection(),
            'lounges' => $this->getCallBackCollection(),
            'championshipCompetitors' => $this->getCallBackCollection(),
            'stageCompetitors' => $this->getCallBackCollection(),
            'loungeCompetitors' => $this->getCallBackCollection(),
        ];
    }

    private function getCallBackCollection(): callable
    {
        return function ($innerObjects) {
            $objects = [];
            if ($innerObjects instanceof Collection) {
                foreach ($innerObjects as $object) {
                    $objects[] = $this->getDataEntity($object);
                }
            }

            return $objects;
        };
    }

    private function getDataEntity(object $object): array
    {
        $data = [];
        if ($object instanceof EntityIdentityInterface) {
            $data['id'] = $object->getId();
            $data['uuid'] = $object->getUuid();
        }

        if ($object instanceof EntityContentInterface) {
            $data['title'] = $object->getTitle();
        }

        return $data;
    }
}
