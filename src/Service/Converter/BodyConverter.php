<?php
/**
 * Created by PhpStorm.
 * User: raulnet
 * Date: 06/01/19
 * Time: 14:57
 */

namespace App\Service\Converter;

use JsonSchema\Exception\ValidationException;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;
use Sensio\Bundle\FrameworkExtraBundle\Request\ParamConverter\ParamConverterInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\Exception\HttpException;
use Symfony\Component\PropertyInfo\Extractor\ReflectionExtractor;
use Symfony\Component\Serializer\Serializer;
use Symfony\Component\Serializer\Normalizer\DateTimeNormalizer;
use Symfony\Component\Serializer\Normalizer\ObjectNormalizer;
use Symfony\Component\Validator\ConstraintViolation;
use Symfony\Component\Validator\Validator\ValidatorInterface;

class BodyConverter implements ParamConverterInterface
{
    private const NAMES = 'body.converter';

    private $validator;

    public function __construct(ValidatorInterface $validator)
    {
        $this->validator = $validator;
    }

    public function apply(Request $request, ParamConverter $configuration)
    {
        try {
            $content = json_decode($request->getContent(), true);
        } catch (\Exception $e) {
            throw new HttpException(409, json_last_error());
        }

        $normalizer = new ObjectNormalizer(null, null, null, new ReflectionExtractor());
        $serializer = new Serializer([new DateTimeNormalizer(), $normalizer]);
        $attribute = $serializer->denormalize($content, $configuration->getClass());
        $this->validate($attribute);
        $request->attributes->set(
            $configuration->getName(),
            $attribute
        );
    }

    public function supports(ParamConverter $configuration): bool
    {
        return self::NAMES === $configuration->getConverter();
    }

    private function validate($attribute): void
    {
        $violations = $this->validator->validate($attribute);
        if ($violations->count() > 0) {
            $message = [];
            /** @var ConstraintViolation $violation */
            foreach ($violations as $violation) {
                $message[] = $violation->getPropertyPath().': '.str_replace('.', '', $violation->getMessage());
            }
            throw new ValidationException(implode(', ', $message).'.', 422);
        }
    }
}
