<?php

namespace App\Service\Converter;

use App\Entity\Lounge;
use App\Entity\Performance;
use App\Entity\Round;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;
use Sensio\Bundle\FrameworkExtraBundle\Request\ParamConverter\ParamConverterInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\Exception\HttpException;
use Symfony\Component\PropertyInfo\Extractor\ReflectionExtractor;
use Symfony\Component\Serializer\Serializer;
use Symfony\Component\Serializer\Normalizer\DateTimeNormalizer;
use Symfony\Component\Serializer\Normalizer\ObjectNormalizer;

class PerformancesConverter implements ParamConverterInterface
{
    private const NAMES = 'performances.converter';

    public function apply(Request $request, ParamConverter $configuration): bool
    {
        try {
            $content = json_decode($request->getContent(), true);
        } catch (\Exception $e) {
            throw new HttpException(409, json_last_error());
        }

        $lounge = $request->attributes->get('lounge');
        $round = $request->attributes->get('round');

        $request->attributes->set(
            $configuration->getName(),
           $this->getValue($lounge, $round, $content, $configuration->getClass())
        );
    }

    public function supports(ParamConverter $configuration): bool
    {
        return self::NAMES === $configuration->getConverter();
    }

    private function getValue(Lounge $lounge, Round $round, array $content, string $className)
    {
        $normalizer = new ObjectNormalizer(null, null, null, new ReflectionExtractor());
        $serializer = new Serializer([new DateTimeNormalizer(), $normalizer]);
        //TODO dec find competitor by lounge and idCompetitor
        /** @var Performance $performances */
        $performances = $serializer->denormalize($content, $className);

        return $performances->setRound($round);
    }

    private function isArrayClass(string $configurationClass): bool
    {
        return '[]' === substr($configurationClass, -2);
    }

    private function getClass(string $configurationClass): string
    {
        return '';
    }
}
