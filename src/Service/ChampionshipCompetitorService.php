<?php

namespace App\Service;

use App\Entity\Championship;
use App\Entity\ChampionshipCompetitor;
use App\Entity\Competitor;
use Doctrine\ORM\EntityManagerInterface;

class ChampionshipCompetitorService
{
    private $manager;

    public function __construct(EntityManagerInterface $manager)
    {
        $this->manager = $manager;
    }

    public function createChampionshipCompetitor(Championship $championship, Competitor $competitor): void
    {
        $championshipCompetitor = new ChampionshipCompetitor();

        $championshipCompetitor->setChampionship($championship);
        $championshipCompetitor->setCompetitor($competitor);
        $this->manager->persist($championshipCompetitor);
        $championship->addChampionshipCompetitor($championshipCompetitor);
        $this->manager->flush();
    }

    public function removeChampionshipCompetitor(Championship $championship, ChampionshipCompetitor $championshipCompetitor): void
    {
        $championship->removeChampionshipCompetitor($championshipCompetitor);
        $championshipCompetitor->setDateDeleted(new \DateTime('now'));
        $this->manager->flush();
    }
}
