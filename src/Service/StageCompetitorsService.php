<?php

namespace App\Service;

use App\Entity\ChampionshipCompetitor;
use App\Entity\Stage;
use App\Entity\StageCompetitor;
use Doctrine\ORM\EntityManagerInterface;

class StageCompetitorsService
{
    private $manager;

    /**
     * StageCompetitorsService constructor.
     *
     * @param $manager
     */
    public function __construct(EntityManagerInterface $manager)
    {
        $this->manager = $manager;
    }

    public function createStageCompetitor(Stage $stage, ChampionshipCompetitor $championshipCompetitor): void
    {
        $stageCompetitor = new StageCompetitor();
        $stageCompetitor->setChampionshipCompetitor($championshipCompetitor);
        $stageCompetitor->setStage($stage);
        $this->manager->persist($stageCompetitor);
        $this->manager->flush();
    }
}
