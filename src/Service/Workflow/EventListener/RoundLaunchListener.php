<?php

namespace App\Service\Workflow\EventListener;

use App\Entity\Round;
use App\Exception\GridException;
use App\Service\RoundService;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\Workflow\Event\Event;
use Symfony\Component\Workflow\Event\GuardEvent;

class RoundLaunchListener implements EventSubscriberInterface
{
    private $roundService;

    public function __construct(RoundService $roundService)
    {
        $this->roundService = $roundService;
    }

    public function guardLaunch(GuardEvent $event): void
    {
        /** @var Round $subject */
        $subject = $event->getSubject();
        if ($subject->isDeleted()) {
            $event->setBlocked(true);
        }
    }

    public function transitionLaunch(Event $event): void
    {
        /** @var Round $subject */
        $subject = $event->getSubject();
        try {
            $this->roundService->bindLoungeCompetitorsToRoundPerformance($subject);
        } catch (GridException $exception) {
            $event->stopPropagation();
        }
    }

    public static function getSubscribedEvents(): array
    {
        return [
            'workflow.round_workflow.guard.launch' => ['guardLaunch'],
            'workflow.round_workflow.transition.launch' => ['transitionLaunch'],
        ];
    }
}
