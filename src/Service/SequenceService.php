<?php

namespace App\Service;

use App\Entity\Interfaces\EntitySequenceCollectionInterface;
use App\Entity\Interfaces\EntitySequenceInterface;
use App\Entity\Round;
use App\Entity\Stage;
use App\Entity\Traits\EntityIdentityTrait;
use App\Exception\GridException;
use App\Repository\RoundRepository;
use App\Repository\StageRepository;
use Doctrine\ORM\EntityManagerInterface;

class SequenceService
{
    private $manager;

    public function __construct(EntityManagerInterface $manager)
    {
        $this->manager = $manager;
    }

    public function removeSequence(
        EntitySequenceCollectionInterface $entity,
        EntitySequenceInterface $entitySequence
    ): void {
        $entity->_removeSequence($entitySequence);
        $collection = $entity->_getSequenceCollection();

        $sequence = 1;
        /** @var EntitySequenceInterface $item */
        foreach ($collection as $item) {
            $item->setSequence($sequence);
            ++$sequence;
        }
    }

    public function changeSequence(
        EntitySequenceCollectionInterface $entity,
        EntitySequenceInterface $entitySequence,
        int $newSequence
    ): void {
        $this->manager->beginTransaction();
        if ($newSequence < 1) {
            throw new GridException('sequence.should.be.more.than.zero');
        }
        $collection = $this->getNewCollectionSequence($entity, $entitySequence, $newSequence);
        /** @var StageRepository|RoundRepository $repository */
        $repository = $this->manager->getRepository($entitySequence->getClassName());
        /** @var Stage|Round $entity */
        $repository->clearAllSequence($entity);
        foreach ($collection as $itemHasSequence) {
            $repository->editSequence($itemHasSequence[0], $itemHasSequence[1]);
        }
        $this->manager->commit();
    }

    private function getNewCollectionSequence(
        EntitySequenceCollectionInterface $entity,
        EntitySequenceInterface $entitySequence,
        int $newSequence
    ): iterable {
        $sequence = 1;
        $collection = $entity->_getSequenceCollection();
        $countItems = $collection->count();
        if ($newSequence > $countItems) {
            $newSequence = $countItems;
        }
        if ($countItems === $newSequence) {
            /** @var EntitySequenceInterface|EntityIdentityTrait $item */
            foreach ($collection as $item) {
                /** @var Stage|Round $entitySequence */
                if ($item->getId() !== $entitySequence->getId()) {
                    yield [$item, $sequence];
                    ++$sequence;
                }
            }
            yield [$entitySequence, $newSequence];
        } else {
            /** @var EntitySequenceInterface|EntityIdentityTrait $item */
            foreach ($collection as $item) {
                if ($item->getId() === $entitySequence->getId()) {
                    yield [$entitySequence, $newSequence];
                    continue;
                }
                if ($sequence === $newSequence) {
                    ++$sequence;
                }
                yield [$item, $sequence];
                ++$sequence;
            }
        }
    }
}
