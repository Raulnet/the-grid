<?php

namespace App\Service;

use App\Entity\Club;
use App\Exception\GridException;
use Doctrine\ORM\EntityManagerInterface;

class ClubService
{
    private $manager;

    /**
     * ClubService constructor.
     *
     * @param $manager
     */
    public function __construct(EntityManagerInterface $manager)
    {
        $this->manager = $manager;
    }

    public function removeClub(Club $club, bool $softDelete = true): void
    {
        if ($club->getCompetitors()->count() > 0) {
            throw new GridException('error.club.contains.competitor');
        }
        if ($softDelete) {
            $club->setDateDeleted(new \DateTime('now'));
        } else {
            $this->manager->remove($club);
        }
        $this->manager->flush();
    }
}
