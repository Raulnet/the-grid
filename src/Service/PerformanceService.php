<?php

namespace App\Service;

use App\Entity\LoungeCompetitor;
use App\Entity\Performance;
use App\Entity\Round;
use Doctrine\ORM\EntityManagerInterface;

class PerformanceService
{
    private $manager;

    public function __construct(EntityManagerInterface $manager)
    {
        $this->manager = $manager;
    }

    public function create(Round $round, LoungeCompetitor $loungeCompetitor): void
    {
        $performance = new Performance();
        $performance->setRound($round);
        $performance->setLoungeCompetitor($loungeCompetitor);
        $this->manager->persist($performance);
        $this->manager->flush();
    }
}
