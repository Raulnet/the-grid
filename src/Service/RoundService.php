<?php

namespace App\Service;

use App\Entity\Enum\RoundEnum;
use App\Entity\LoungeCompetitor;
use App\Entity\Round;
use App\Entity\Stage;
use App\Exception\GridException;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\HttpKernel\Exception\HttpException;
use Symfony\Component\Workflow\Exception\TransitionException;
use Symfony\Component\Workflow\Registry;

class RoundService
{
    private $manager;
    private $sequenceService;
    private $performanceService;
    private $registry;

    public function __construct(
        EntityManagerInterface $manager,
        SequenceService $sequenceService,
        PerformanceService $performanceService,
        Registry $registry
    ) {
        $this->manager = $manager;
        $this->sequenceService = $sequenceService;
        $this->performanceService = $performanceService;
        $this->registry = $registry;
    }

    public function createRound(Stage $stage, Round $round): void
    {
        $stage->addRound($round);

        if (null !== $round->getDateStart()) {
            // TODO add edit by startdate
        }
        $this->manager->persist($round);
        $this->manager->flush();
    }

    public function editRound(Round $round): void
    {
        if (null !== $round->getDateStart()) {
            // TODO add edit by startdate
        }
        $this->manager->flush();
    }

    public function editSequence(Round $round, int $sequence): void
    {
        if (null !== $round->getDateStart()) {
            throw new GridException('exception.sequence.could.not.be.change.remove.start.date.before');
        }
        $this->sequenceService->changeSequence($round->getStage(), $round, $sequence);
        $this->manager->flush();
    }

    public function removeRound(Round $round): void
    {
        $this->sequenceService->removeSequence($round->getStage(), $round);
        $this->manager->flush();
    }

    public function launch(Round $round): void
    {
        $this->manager->beginTransaction();
        $workflow = $this->registry->get($round);
        try {
            $workflow->apply($round, RoundEnum::TRANSITION_LAUNCH);
        } catch (TransitionException $exception) {
            throw new HttpException(422, 'round could not be launch.');
        }
        $this->manager->flush();
        $this->manager->commit();
    }

    public function bindLoungeCompetitorsToRoundPerformance(Round $round): void
    {
        $loungeCompetitors = $this->manager->getRepository(LoungeCompetitor::class)->findByRound($round);
        foreach ($loungeCompetitors as $loungeCompetitor) {
            $this->performanceService->create($round, $loungeCompetitor[0]);
        }
    }
}
