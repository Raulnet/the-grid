<?php
/**
 * Created by PhpStorm.
 * User: raulnet
 * Date: 06/01/19
 * Time: 20:34
 */

namespace App\Service;

use App\Exception\HydratorException;

class ObjectHydrator
{
    private const SERVER_TIME_ZONE = 'UTC';
    private const FIELD_IGNORED = [
        'id', 'uuid', 'stages', 'rounds', 'lounges', 'dateDeleted', 'dateCreation', 'dateUpdate', 'sequence', 'currentPlace',
    ];

    private $callable = [];
    private $timeZone;

    public function __construct()
    {
        $this->loadCallable();
        $this->timeZone = new \DateTimeZone('UTC');
    }

    public function hydrate(object $object, string $jsonData): void
    {
        $data = json_decode($jsonData, true);
        if (!\is_array($data)) {
            throw new HydratorException(json_last_error(), 409);
        }
        $this->hydrateObject($object, $data);
    }

    private function hydrateObject(object $object, array $data): void
    {
        if (array_key_exists('timeZone', $data)) {
            $this->timeZone = new \DateTimeZone($data['timeZone']);
        }
        foreach ($data as $key => $value) {
            if (\in_array($key, self::FIELD_IGNORED, true)) {
                continue;
            }
            $method = 'set'.ucfirst($key);
            if (!method_exists($object, $method)) {
                continue;
            }
            if (array_key_exists($key, $this->callable)) {
                $value = $this->callable[$key]($value);
            }
            $object->{$method}($value);
        }
    }

    private function loadCallable(): void
    {
        $dateTimeCallable = function (string $value) {
            if ('' === $value) {
                return null;
            }
            $date = new \DateTime($value, $this->timeZone);

            return $date->setTimezone(new \DateTimeZone(self::SERVER_TIME_ZONE));
        };

        $this->callable['dateStart'] = $dateTimeCallable;
        $this->callable['dateEnd'] = $dateTimeCallable;
    }
}
