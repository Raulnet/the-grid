<?php
/**
 * Created by PhpStorm.
 * User: raulnet
 * Date: 29/12/18
 * Time: 19:53
 */

namespace App\Service;

use App\Entity\User;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;

class UserService
{
    private $manager;
    private $userPasswordEncoder;

    /**
     * UserService constructor.
     */
    public function __construct(EntityManagerInterface $manager, UserPasswordEncoderInterface $userPasswordEncoder)
    {
        $this->manager = $manager;
        $this->userPasswordEncoder = $userPasswordEncoder;
    }

    public function registerUser(User $user): void
    {
        $encoded = $this->userPasswordEncoder->encodePassword($user, $user->getPassword());
        $user->setPassword($encoded);
        $this->manager->persist($user);
        $this->manager->flush();
    }
}
