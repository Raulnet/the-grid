Feature: test api browser
  Scenario: I'am a user and i don't have a token
    Given fixtures "user.yml"
    When I am on "/api/championships"
    Then the response status code should be 401
  Scenario: I'am a userAdmin and i am have a token
    Given fixtures "user.yml"
    Given I am authenticate on Api as "admin@thegrid.com" with password "admin"
    When I am on "/api/championships"
    Then the response status code should be 200
