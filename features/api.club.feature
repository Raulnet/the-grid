Feature: test api Clubs browser
  Scenario: I'am a user and i want to see all clubs
    Given fixtures "user.yml,club.yml"
    When I am on "/api/clubs"
    Then the response status code should be 401
    Given I am authenticate on Api as "admin@thegrid.com" with password "admin"
    When I am on "/api/clubs"
    Then the response status code should be 200
  Scenario: I'am a user and i want to see one club by id
    Given fixtures "user.yml,club.yml"
    When I am on "/api/clubs/1"
    Then the response status code should be 401
    Given I am authenticate on Api as "admin@thegrid.com" with password "admin"
    When I am on "/api/clubs/1"
    Then the response status code should be 200
  Scenario: I'am a user and i want to delete one club by id
    Given fixtures "user.yml,club.yml"
    When I send a "DELETE" request on "/api/clubs/3"
    Then the response status code should be 401
    Given I am authenticate on Api as "admin@thegrid.com" with password "admin"
    When I send a "DELETE" request on "/api/clubs/3"
    Then the response status code should be 204
  Scenario: I'am a user and i want to create a club
    Given fixtures "user.yml,club.yml"
    When I send a "POST" request on "/api/clubs" with content body:
    Then the response status code should be 401
    Given I am authenticate on Api as "admin@thegrid.com" with password "admin"
    When I send a "POST" request on "/api/clubs" with content body:
      """
      {"title": "test", "description": "test content description"}
      """
    Then the response status code should be 201
  Scenario: I'am a user and i want to club a club
    Given fixtures "user.yml,club.yml"
    When I send a "PATCH" request on "/api/clubs/1" with content body:
    Then the response status code should be 401
    Given I am authenticate on Api as "admin@thegrid.com" with password "admin"
    When I send a "PATCH" request on "/api/clubs/1" with content body:
      """
      {"title": "test edited", "description": "edited content description"}
      """
    Then the response status code should be 204

