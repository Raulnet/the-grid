Feature: test api Championships browser
  Scenario: I'am a user and i want to see all championships
    Given fixtures "user.yml,club.yml,competitor.yml,championship1.yml"
    When I am on "/api/championships"
    Then the response status code should be 401
    Given I am authenticate on Api as "admin@thegrid.com" with password "admin"
    When I am on "/api/championships"
    Then the response status code should be 200
  Scenario:  I'am a user and i want to see one championship
    Given fixtures "user.yml,club.yml,competitor.yml,championship1.yml"
    When I am on "/api/championships/1"
    Then the response status code should be 401
    Given I am authenticate on Api as "admin@thegrid.com" with password "admin"
    When I am on "/api/championships/1"
    Then the response status code should be 200
