<?php

use Behat\Behat\Context\Context;
use Symfony\Component\HttpKernel\KernelInterface;

class DataBaseContext implements Context
{
    private const FOLDER_FIXTURES = '/fixtures/';

    /**
     * @var KernelInterface
     */
    private $kernel;

    public function __construct(KernelInterface $kernel)
    {
        $this->kernel = $kernel;
    }

    /**
     * @BeforeScenario
     */
    public function clearDataBase(): void
    {
        $container = $this->kernel->getContainer();
        if (null === $container) {
            throw new RuntimeException('Container could not be NULL');
        }
        $connection = $container->get('doctrine.orm.entity_manager')->getConnection();
        $connection->getConfiguration()->setSQLLogger(null);
        $connection->prepare('SET FOREIGN_KEY_CHECKS = 0;')->execute();

        foreach ($connection->getSchemaManager()->listTableNames() as $tableNames) {
            if ('migration_version' === $tableNames) {
                continue;
            }
            $sql = 'TRUNCATE TABLE '.$tableNames;
            $connection->prepare($sql)->execute();
        }

        $connection->prepare('SET FOREIGN_KEY_CHECKS = 1;')->execute();
    }

    /**
     * @Given /^(?:|Load )fixtures "(?P<text>(?:[^"]|\\")*)"$/
     */
    public function loadFixture(string $fileNames): void
    {
        $fileNames = $this->getFileNames($fileNames);
        $container = $this->kernel->getContainer();
        if (null === $container) {
            throw new RuntimeException('Container could not be NULL');
        }
        $container->get('fidry_alice_data_fixtures.loader.doctrine')->load($fileNames);
    }

    private function getFileNames(string $filenames): array
    {
        $list = [];
        $fileNames = explode(',', $filenames);
        foreach ($fileNames as $fileName) {
            $fileName = $this->kernel->getProjectDir().self::FOLDER_FIXTURES.$fileName;
            if (!file_exists($fileName)) {
                throw new RuntimeException($fileName.' not found');
            }
            $list[] = $fileName;
        }

        return $list;
    }
}
