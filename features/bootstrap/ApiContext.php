<?php

use Behat\MinkExtension\Context\RawMinkContext;
use Symfony\Component\HttpKernel\KernelInterface;
use App\Entity\User;
use Symfony\Component\HttpFoundation\Request;
use Behat\Gherkin\Node\PyStringNode;

class ApiContext extends RawMinkContext
{
    private $jwtToken;
    private $request;

    use \Behat\Symfony2Extension\Context\KernelDictionary;

    public function __construct(KernelInterface $kernel)
    {
        $this->kernel = $kernel;
    }

    /**
     * @Given /^(?:|I )am authenticate on Api as "(?P<email>(?:[^"]|\\")*)" with password "(?P<password>(?:[^"]|\\")*)"$/
     */
    public function iAmAuthenticatedOnApiAs(string $email, string $password): void
    {
        $container = $this->kernel->getContainer();
        if (null === $container) {
            throw new RuntimeException('Container could not be NULL');
        }
        /** @var User $user */
        $user = $container->get('doctrine.orm.entity_manager')->getRepository(
            User::class
        )->findOneBy([
            'email' => $email,
            'dateDeleted' => null,
        ]);

        if (null === $user) {
            throw new RuntimeException("user $email not found");
        }
        $request = Request::create('/api/login', Request::METHOD_POST, [], [], [], [], json_encode([
                'email' => $user->getEmail(),
                'password' => $password,
                'apiKey' => $user->getApiKey(),
            ])
        );

        $response = $this->getKernel()->handle($request);
        $content = json_decode($response->getContent(), true);
        if (201 !== $response->getStatusCode()) {
            if (\is_array($content)) {
                $content = $content['message'];
            }
            throw new RuntimeException($content);
        }
        $this->jwtToken = $content['token'];
        $this->getSession()->setRequestHeader('Authorization', 'Bearer '.$content['token']);
    }

    /**
     * @When /^(?:|I ) don't have jwt-token$/
     */
    public function iDontHavJwtToken(): void
    {
        $this->jwtToken = null;
        $this->getSession()->setRequestHeader('Authorization', null);
    }

    /**
     * @Given /^(?:|I )send a "(?P<method>(?:[^"]|\\")*)" request on "(?P<url>(?:[^"]|\\")*)"$/
     */
    public function iSendARequestTo($method, $url): void
    {
        $this->sendRequest($method, $url);
    }

    /**
     * @Given /^(?:|I )send a "(?P<method>(?:[^"]|\\")*)" request on "(?P<url>(?:[^"]|\\")*)" with content body:$/
     */
    public function iSendARequestOnPathWithContent(string $method, string $url, PyStringNode $body = null): void
    {
        $this->sendRequest($method, $url, $body);
    }

    private function sendRequest(string $method, string $path, PyStringNode $body = null): void
    {
        $headers = [];
        if (null !== $this->jwtToken) {
            $headers['HTTP_AUTHORIZATION'] = $this->jwtToken;
        }
        /** @var \Symfony\Component\BrowserKit\Client $client */
        $client = $this->getSession()->getDriver()->getClient();
        $this->getSession()->setRequestHeader('Authorization', 'Bearer '.$this->jwtToken);
        $client->request($method, $path, [], [], $headers, null !== $body ? $body->getRaw() : null);
    }
}
