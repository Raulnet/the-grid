<?php

use App\Entity\User;
use Behat\MinkExtension\Context\RawMinkContext;
use Symfony\Component\HttpKernel\KernelInterface;
use Symfony\Component\Security\Core\Authentication\Token\UsernamePasswordToken;

class AuthContext extends RawMinkContext
{
    private $request;

    use \Behat\Symfony2Extension\Context\KernelDictionary;

    public function __construct(KernelInterface $kernel)
    {
        $this->kernel = $kernel;
    }

    /**
     * @Given /^(?:|I )am authenticate as "(?P<username>(?:[^"]|\\")*)"$/
     */
    public function authenticateAs(string $username): void
    {
        $this->visitPath('/admin'); // init security context
        $this->authentication($username);
    }

    /**
     * @Given /^(?:|I )am authenticate as "(?P<username>(?:[^"]|\\")*)" on "(?P<path>(?:[^"]|\\")*)"$/
     */
    public function authenticateOn(string $username, string $path): void
    {
        $this->visitPath('/admin'); // init security context
        $this->authentication($username);
        $this->visitPath($path);
    }

    private function authentication(string $username): void
    {
        $firewallName = 'main';
        $container = $this->kernel->getContainer();
        if (null === $container) {
            throw new RuntimeException('Container could not be NULL');
        }
        $session = $container->get('session');
        /** @var User $user */
        $user = $container->get('doctrine.orm.entity_manager')->getRepository(
            User::class
        )->findOneBy([
            'username' => $username,
            'email' => $username.'@thegrid.com',
        ]);

        if (null === $user) {
            throw new RuntimeException("user $username not found");
        }

        $token = new UsernamePasswordToken($user, null, $firewallName, $user->getRoles());
        $container->get('security.token_storage')->setToken($token);
        $session->set('_security_'.'main', serialize($token));
        $session->save();

        $granted = $container->get('security.authorization_checker')->isGranted('ROLE_USER');

        if (!$granted) {
            throw new RuntimeException('User is not logged in');
        }
    }
}
