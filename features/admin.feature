Feature: behavior Admin Page
  Scenario: I'am User Without authentication i go to the AdminPage
    Given fixtures "user.yml,club.yml,competitor.yml"
    When I am on "/admin"
    Then I should be on "/login"
    And the response status code should be 200
    Then I fill in "email" with "admin@thegrid.com"
    And I fill in "password" with "admin"
    And I press "Sign in"
    Then I should be on "/admin/dashboard"
    When I am on "/logout"
    Then I should be on "/"
    And the response status code should be 200
