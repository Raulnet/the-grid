Feature: test Firewall security context
  Scenario: Test Firewall User On admin page
    Given fixtures "user.yml"
    Given I am authenticate as "user" on "/admin"
    Then the response status code should be 403

  Scenario: Test fireWall Admin on admin page
    Given fixtures "user.yml"
    Given I am authenticate as "admin" on "/admin"
    When the response status code should be 200
    Then I should be on "/admin/dashboard"
