<?php
/**
 * Created by PhpStorm.
 * User: raulnet
 * Date: 05/01/19
 * Time: 21:28
 */

namespace App\Tests\Controller;

use PHPUnit\Framework\MockObject\RuntimeException;
use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;
use Symfony\Bundle\FrameworkBundle\Client;

abstract class AbstractTestCase extends WebTestCase
{
    private const FOLDER_FIXTURES = '/fixtures/';
    /** @var Client */
    protected $client;

    public function setUp()
    {
        parent::setUp();
        $this->client = static::createClient();
        static::$container = $this->client->getContainer();
    }

    public function truncateTableDataBase(): void
    {
        $container = static::$container;
        if (null === $container) {
            throw new RuntimeException('Container could not be NULL');
        }
        $connection = $container->get('doctrine.orm.entity_manager')->getConnection();
        $connection->getConfiguration()->setSQLLogger();
        $connection->prepare('SET FOREIGN_KEY_CHECKS = 0;')->execute();

        foreach ($connection->getSchemaManager()->listTableNames() as $tableNames) {
            if ('migration_version' === $tableNames) {
                continue;
            }
            $sql = 'TRUNCATE TABLE '.$tableNames;
            $connection->prepare($sql)->execute();
        }

        $this->assertTrue($connection->prepare('SET FOREIGN_KEY_CHECKS = 1;')->execute());
    }

    public function loadFixtures(array $fileNames): void
    {
        $container = static::$container;
        if (null === $container) {
            throw new RuntimeException('Container could not be NULL');
        }
        $container->get('fidry_alice_data_fixtures.loader.doctrine')->load($this->getFileNames($fileNames));
    }

    private function getFileNames(array $filenames): array
    {
        $list = [];
        foreach ($filenames as $fileName) {
            $fileName = static::$kernel->getProjectDir().self::FOLDER_FIXTURES.$fileName;
            if (!file_exists($fileName)) {
                throw new RuntimeException($fileName.' not found');
            }
            $list[] = $fileName;
        }

        return $list;
    }
}
