<?php
/**
 * Created by PhpStorm.
 * User: raulnet
 * Date: 02/01/19
 * Time: 02:46
 */

namespace App\Tests\Controller;

class SecurityControllerTest extends AbstractTestCase
{
    public function testApiLogin(): void
    {
        $this->truncateTableDataBase();
        $this->loadFixtures([
            'user.yml',
        ]);

        $this->client->request('GET', '/login', [], [], []);

        $this->assertSame(200, $this->client->getResponse()->getStatusCode());
    }
}
