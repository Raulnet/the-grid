<?php
/**
 * Created by PhpStorm.
 * User: raulnet
 * Date: 06/01/19
 * Time: 12:52
 */

namespace App\Tests\Controller\dataFixtures;

class AuthToken
{
    public static $authToken = [
        'superadmin' => [
            'id' => 1,
            'user_id' => 1,
            'token' => 'superadminToken',
            'uuid' => '00000000-0000-0000-0000-000000000001',
            'expiration_time' => '2999-12-31 00:00:00',
        ],
        'admin' => [
            'id' => 2,
            'user_id' => 2,
            'token' => 'adminToken',
            'uuid' => '00000000-0000-0000-0000-000000000002',
            'expiration_time' => '2999-12-31 00:00:00',
        ],
        'user' => [
            'id' => 3,
            'user_id' => 3,
            'token' => 'userToken',
            'uuid' => '00000000-0000-0000-0000-000000000003',
            'expiration_time' => '2999-12-31 00:00:00',
        ],
        'user2' => [
            'id' => 4,
            'user_id' => 4,
            'token' => 'user2Token',
            'uuid' => '00000000-0000-0000-0000-000000000004',
            'expiration_time' => '2999-12-31 00:00:00',
        ],
    ];
}
