<?php
/**
 * Created by PhpStorm.
 * User: raulnet
 * Date: 07/01/19
 * Time: 22:31
 */

namespace App\Tests\Controller\Api;

class ApiCompetitorControllerTest extends AbstractApiTestCase
{
    public function testCompetitors(): void
    {
        $this->truncateTableDataBase();
        $this->loadFixtures(['user.yml', 'competitor.yml', 'club.yml']);
        $this->sendMultiRequest($this->getRequests());
    }

    private function getRequests(): iterable
    {
        $adminHeader = $this->getAuthHeader('admin');
        yield ['GET', '/api/competitors', 401];
        yield ['GET', '/api/competitors', 200, $adminHeader];

        yield ['GET', '/api/competitors/1', 401];
        yield ['GET', '/api/competitors/1', 200, $adminHeader];
        yield ['GET', '/api/competitors/0', 404, $adminHeader];

        $data = json_encode([
            'username' => 'TESTUNIT',
            'country' => 'FR',
            'bid' => 'TEST0001',
        ]);
        yield ['POST', '/api/competitors', 401];
        yield ['POST', '/api/competitors', 201, $adminHeader, $data];

        $data = json_encode([
            'username' => 'TESTUNIT',
            'country' => 'GB',
            'bid' => 'T0001',
        ]);
        yield ['PUT', '/api/competitors/1', 401];
        yield ['PUT', '/api/competitors/1', 204, $adminHeader, $data];
        yield ['PUT', '/api/competitors/0', 404, $adminHeader, $data];

        yield ['DELETE', '/api/competitors/1', 401];
        yield ['DELETE', '/api/competitors/1', 204, $adminHeader];
        yield ['DELETE', '/api/competitors/1', 404, $adminHeader];
        yield ['DELETE', '/api/competitors/0', 404, $adminHeader];

        yield ['DELETE', '/api/competitors/1/clubs/1', 401];
        yield ['DELETE', '/api/competitors/2/clubs/1', 204, $adminHeader];
        yield ['DELETE', '/api/competitors/1/clubs/0', 404, $adminHeader];
        yield ['DELETE', '/api/competitors/0/clubs/1', 404, $adminHeader];

        yield ['PUT', '/api/competitors/1/clubs/1', 401];
        yield ['PUT', '/api/competitors/2/clubs/1', 204, $adminHeader];
        yield ['PUT', '/api/competitors/1/clubs/0', 404, $adminHeader];
        yield ['PUT', '/api/competitors/0/clubs/1', 404, $adminHeader];
    }
}
