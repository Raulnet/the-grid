<?php

namespace App\Tests\Controller\Api;

class ApiPerformancesControllerTest extends AbstractApiTestCase
{
    public function testRequestController(): void
    {
        $this->truncateTableDataBase();
        $this->loadFixtures(['user.yml', 'competitor.yml', 'club.yml', 'championship1.yml']);
        $this->sendMultiRequest($this->getRequests());
    }

    private function getRequests(): iterable
    {
        $adminHeader = $this->getAuthHeader('admin');
        yield ['GET', '/api/lounges/1/rounds/1/performances', 401];
        yield ['GET', '/api/lounges/1/rounds/1/performances', 200, $adminHeader];
        yield ['GET', '/api/lounges/0/rounds/1/performances', 404, $adminHeader];
        yield ['GET', '/api/lounges/1/rounds/0/performances', 404, $adminHeader];

        yield ['GET', '/api/lounges/1/performances', 401];
        yield ['GET', '/api/lounges/1/performances', 200, $adminHeader];
        yield ['GET', '/api/lounges/0/performances', 404, $adminHeader];
    }
}
