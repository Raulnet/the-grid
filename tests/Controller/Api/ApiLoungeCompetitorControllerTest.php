<?php

namespace App\Tests\Controller\Api;

class ApiLoungeCompetitorControllerTest extends AbstractApiTestCase
{
    public function testLoungeCompetitors(): void
    {
        $this->truncateTableDataBase();
        $this->loadFixtures(['user.yml', 'competitor.yml', 'club.yml', 'championship1.yml']);
        $this->sendMultiRequest($this->getRequests());
    }

    private function getRequests(): iterable
    {
        $adminHeader = $this->getAuthHeader('admin');
        yield ['GET', '/api/lounges/1/competitors', 401];
        yield ['GET', '/api/lounges/1/competitors', 200, $adminHeader];
        yield ['GET', '/api/lounges/0/competitors', 404, $adminHeader];

        yield ['GET', '/api/lounges/1/competitors/1', 401];
        yield ['GET', '/api/lounges/1/competitors/1', 200, $adminHeader];
        yield ['GET', '/api/lounges/0/competitors/1', 404, $adminHeader];
        yield ['GET', '/api/lounges/1/competitors/0', 404, $adminHeader];

        yield ['POST', '/api/lounges/1/competitors/1', 401];
        yield ['POST', '/api/lounges/1/competitors/1', 409, $adminHeader];

        yield ['POST', '/api/lounges/1/competitors/2', 201, $adminHeader];
        yield ['GET', '/api/lounges/1/competitors/2', 200, $adminHeader];

        yield ['POST', '/api/lounges/0/competitors/1', 404, $adminHeader];
        yield ['POST', '/api/lounges/1/competitors/0', 404, $adminHeader];

        yield ['DELETE', '/api/lounges/1/competitors/1', 401];

        yield ['DELETE', '/api/lounges/1/competitors/2', 204, $adminHeader];
        yield ['GET', '/api/lounges/1/competitors/2', 404, $adminHeader];

        yield ['DELETE', '/api/lounges/0/competitors/1', 404, $adminHeader];
        yield ['DELETE', '/api/lounges/1/competitors/0', 404, $adminHeader];
    }
}
