<?php

namespace App\Tests\Controller\Api;

class ApiChampionshipCompetitorTest extends AbstractApiTestCase
{
    public function testChampionshipCompetitors(): void
    {
        $this->truncateTableDataBase();
        $this->loadFixtures(['user.yml', 'competitor.yml', 'club.yml', 'championship1.yml']);
        $this->sendMultiRequest($this->getRequest());
    }

    private function getRequest(): iterable
    {
        $adminHeader = $this->getAuthHeader('admin');
        yield ['GET', '/api/championships/1/competitors', 401, [], ''];
        yield ['GET', '/api/championships/1/competitors', 200, $adminHeader, ''];
        yield ['GET', '/api/championships/0/competitors', 404, $adminHeader, ''];
        yield ['GET', '/api/championships/1/competitors/1', 401, [], ''];
        yield ['GET', '/api/championships/1/competitors/1', 200, $adminHeader, ''];
        yield ['GET', '/api/championships/0/competitors/1', 404, $adminHeader, ''];
        yield ['GET', '/api/championships/1/competitors/0', 404, $adminHeader, ''];
        yield ['POST', '/api/championships/1/competitors/19', 401, [], ''];
        yield ['POST', '/api/championships/1/competitors/18', 201, $adminHeader, ''];
        yield ['POST', '/api/championships/0/competitors/1', 404, $adminHeader, ''];
        yield ['POST', '/api/championships/1/competitors/0', 404, $adminHeader, ''];
        yield ['DELETE', '/api/championships/1/competitors/1', 401, [], ''];
        yield ['DELETE', '/api/championships/1/competitors/1', 204, $adminHeader, ''];
        yield ['DELETE', '/api/championships/0/competitors/1', 404, $adminHeader, ''];
        yield ['DELETE', '/api/championships/1/competitors/0', 404, $adminHeader, ''];
    }
}
