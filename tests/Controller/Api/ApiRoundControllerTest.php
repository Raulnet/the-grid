<?php

namespace App\Tests\Controller\Api;

class ApiRoundControllerTest extends AbstractApiTestCase
{
    public function testRoutesController(): void
    {
        $this->truncateTableDataBase();
        $this->loadFixtures(['user.yml', 'competitor.yml', 'club.yml', 'championship1.yml']);
        $this->sendMultiRequest($this->getRequests());
    }

    private function getRequests(): iterable
    {
        $adminHeader = $this->getAuthHeader('admin');
        yield ['GET', '/api/stages/1/rounds', 401];
        yield ['GET', '/api/stages/1/rounds', 200, $adminHeader];
        yield ['GET', '/api/stages/0/rounds', 404, $adminHeader];

        yield ['GET', '/api/stages/1/rounds/1', 401];
        yield ['GET', '/api/stages/1/rounds/1', 200, $adminHeader];
        yield ['GET', '/api/stages/0/rounds/1', 404, $adminHeader];
        yield ['GET', '/api/stages/1/rounds/0', 404, $adminHeader];

        $round = json_encode([
            'title' => 'test round',
            'description' => 'test description round',
        ]);
        yield ['POST', '/api/stages/1/rounds', 401];
        yield ['POST', '/api/stages/1/rounds', 201, $adminHeader, $round];
        yield ['POST', '/api/stages/0/rounds', 404, $adminHeader, $round];

        $round = json_encode([
            'title' => 'test edit round',
            'description' => 'test edit description round',
            'wrongProperty' => 'for test hydrator',
            'dateStart' => '2020-01-01 01:00:00',
            'dateEnd' => '',
            'timeZone' => 'Europe/Paris',
        ]);
        yield ['PATCH', '/api/stages/1/rounds/1', 401];
        yield ['PATCH', '/api/stages/1/rounds/1', 204, $adminHeader, $round];
        yield ['PATCH', '/api/stages/0/rounds/1', 404, $adminHeader, $round];

        yield ['PATCH', '/api/stages/1/rounds/2/sequence/1', 401];
        yield ['PATCH', '/api/stages/1/rounds/2/sequence/0', 500, $adminHeader, $round];
        yield ['PATCH', '/api/stages/1/rounds/1/sequence/4', 500, $adminHeader, $round];
        yield ['PATCH', '/api/stages/1/rounds/2/sequence/4', 204, $adminHeader, $round];
        yield ['PATCH', '/api/stages/1/rounds/2/sequence/3', 204, $adminHeader, $round];
        yield ['PATCH', '/api/stages/1/rounds/2/sequence/9999', 204, $adminHeader, $round];
        yield ['PATCH', '/api/stages/1/rounds/2/sequence/2', 204, $adminHeader, $round];
        yield ['PATCH', '/api/stages/1/rounds/2/sequence/1', 204, $adminHeader, $round];
        yield ['PATCH', '/api/stages/0/rounds/3/sequence/1', 404, $adminHeader, $round];
        yield ['PATCH', '/api/stages/1/rounds/0/sequence/1', 404, $adminHeader, $round];

        yield ['DELETE', '/api/stages/1/rounds/2', 401];
        yield ['DELETE', '/api/stages/1/rounds/2', 204, $adminHeader, $round];
        yield ['DELETE', '/api/stages/0/rounds/2', 404, $adminHeader, $round];
        yield ['DELETE', '/api/stages/1/rounds/0', 404, $adminHeader, $round];
    }
}
