<?php
/**
 * Created by PhpStorm.
 * User: raulnet
 * Date: 06/01/19
 * Time: 12:05
 */

namespace App\Tests\Controller\Api;

use App\Entity\User;
use App\Security\ApiTokenProvider;
use App\Tests\Controller\AbstractTestCase;
use App\Tests\Controller\dataFixtures\AuthToken;

abstract class AbstractApiTestCase extends AbstractTestCase
{
    private const REQUEST_METHOD = 0;
    private const REQUEST_PATCH = 1;
    private const REQUEST_STATUS_CODE = 2;
    private const REQUEST_HEADER = 3;
    private const REQUEST_CONTENT = 4;

    public function getToken(string $username): string
    {
        $tokenProvider = new ApiTokenProvider('cd370916-5f8a-4803-bb61-2f0a12c2057b');

        $data = AuthToken::$authToken[$username];

        $user = new User();
        $user->setUsername($username);
        $user->setUuid($data['uuid']);

        $authToken = $tokenProvider->getAuthToken($user);
        $authToken->setUuid($data['uuid']);
        $authToken->setExpirationTime(new \DateTime($data['expiration_time']));
        $authToken->setToken($data['token']);

        return $tokenProvider->getToken($authToken);
    }

    public function getAuthHeader(string $username): array
    {
        return ['HTTP_AUTHORIZATION' => 'Bearer '.$this->getToken($username)];
    }

    protected function sendRequest(string $method, string $path, int $statusCode, array $header = [], ?string $bodyContent = null): void
    {
        $this->client->request($method, $path, [], [], $header, $bodyContent);
        $response = $this->client->getResponse();
        $this->assertSame($statusCode, $response->getStatusCode(), $response->getContent().json_encode([$method, $path, $statusCode]));
    }

    public function sendMultiRequest(iterable $listRequest): void
    {
        foreach ($listRequest as $request) {
            $this->sendRequest(
                $request[self::REQUEST_METHOD],
                $request[self::REQUEST_PATCH],
                $request[self::REQUEST_STATUS_CODE],
                $request[self::REQUEST_HEADER] ?? [],
                $request[self::REQUEST_CONTENT] ?? ''
            );
        }
    }
}
