<?php

namespace App\Tests\Controller\Api;

class ApiStagesControllerTest extends AbstractApiTestCase
{
    public function testStages(): void
    {
        $this->truncateTableDataBase();
        $this->loadFixtures(['user.yml', 'competitor.yml', 'club.yml', 'championship1.yml']);
        $this->sendMultiRequest($this->getRequests());
    }

    private function getRequests(): iterable
    {
        $adminHeader = $this->getAuthHeader('admin');

        yield ['GET', '/api/championships/1/stages', 401];
        yield ['GET', '/api/championships/1/stages', 200, $adminHeader];
        yield ['GET', '/api/championships/0/stages', 404, $adminHeader];

        yield ['GET',  '/api/stages/1', 401];
        yield ['GET',  '/api/stages/1', 200, $adminHeader];
        yield ['GET',  '/api/stages/0', 404, $adminHeader];

        $stage = json_encode([
            'title' => 'test create stage',
            'description' => 'test description create stage',
        ]);

        yield ['POST',  '/api/championships/1/stages', 401, [], $stage];
        yield ['POST',  '/api/championships/1/stages', 201, $adminHeader, $stage];
        yield ['POST',  '/api/championships/0/stages', 404, $adminHeader, $stage];

        $stage = json_encode([
            'title' => 'test Edit stage',
            'description' => 'test description Edited stage',
        ]);

        yield ['PATCH',  '/api/championships/1/stages/1', 401, [], $stage];
        yield ['PATCH',  '/api/championships/1/stages/1', 204, $adminHeader, $stage];
        yield ['PATCH',  '/api/championships/0/stages/1', 404, $adminHeader, $stage];
        yield ['PATCH',  '/api/championships/1/stages/0', 404, $adminHeader, $stage];

        yield ['DELETE',  '/api/championships/1/stages/2', 401, [], $stage];
        yield ['DELETE',  '/api/championships/1/stages/2', 204, $adminHeader, $stage];
        yield ['DELETE',  '/api/championships/0/stages/2', 404, $adminHeader, $stage];
        yield ['DELETE',  '/api/championships/1/stages/0', 404, $adminHeader, $stage];
    }
}
