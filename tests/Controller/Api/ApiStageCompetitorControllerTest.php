<?php

namespace App\Tests\Controller\Api;

class ApiStageCompetitorControllerTest extends AbstractApiTestCase
{
    public function testStagesCompetitors(): void
    {
        $this->truncateTableDataBase();
        $this->loadFixtures(['user.yml', 'competitor.yml', 'club.yml', 'championship1.yml']);
        $this->sendMultiRequest($this->competitorsRequestProvider());
    }

    public function competitorsRequestProvider(): ?\Generator
    {
        $header = $this->getAuthHeader('admin');
        yield ['GET', '/api/stages/1/competitors', 401];
        yield ['GEt', '/api/stages/1/competitors', 200, $header];
        yield ['GET', '/api/stages/0/competitors', 404, $header];
        yield ['GET', '/api/stages/1/competitors/1', 401];
        yield ['GEt', '/api/stages/1/competitors/1', 200, $header];
        yield ['GET', '/api/stages/0/competitors/1', 404, $header];
        yield ['GET', '/api/stages/1/competitors/0', 404, $header];
        yield ['POST', '/api/stages/1/competitors/17', 401];
        yield ['POST', '/api/stages/1/competitors/17', 201, $header];
        yield ['POST', '/api/stages/1/competitors/1', 409, $header];
        yield ['POST', '/api/stages/0/competitors/1', 404, $header];
        yield ['POST', '/api/stages/1/competitors/0', 404, $header];
    }
}
