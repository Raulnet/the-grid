<?php
/**
 * Created by PhpStorm.
 * User: raulnet
 * Date: 07/01/19
 * Time: 01:09
 */

namespace App\Tests\Controller\Api;

class ApiControllerTest extends AbstractApiTestCase
{
    public function testRefresh(): void
    {
        $this->truncateTableDataBase();
        $this->loadFixtures(['user.yml']);
        $this->sendRequest('POST', '/api/refresh', 401);
        $this->sendRequest('POST', '/api/refresh', 201, $this->getAuthHeader('admin'));
    }
}
