<?php

namespace App\Tests\Controller\Api;

class ApiClubControllerTest extends AbstractApiTestCase
{
    public function testClubs(): void
    {
        $this->truncateTableDataBase();
        $this->loadFixtures(['user.yml', 'competitor.yml', 'club.yml']);
        $this->sendMultiRequest($this->getRequests());
    }

    private function getRequests(): iterable
    {
        $adminHeader = $this->getAuthHeader('admin');
        yield ['GET', '/api/clubs', 401];
        yield ['GET', '/api/clubs', 200, $adminHeader];
        yield ['GET', '/api/clubs/1', 401];
        yield ['GET', '/api/clubs/1', 200, $adminHeader];
        yield ['GET', '/api/clubs/0', 404, $adminHeader];
        yield ['POST', '/api/clubs', 401];
        $club = [
            'title' => 'Club test',
            'description' => 'description test',
        ];
        yield ['POST', '/api/clubs', 201, $adminHeader, json_encode($club)];
        yield ['PATCH', '/api/clubs/1', 401];
        $club = [
            'title' => 'Club test edited',
            'description' => 'description test',
        ];
        yield ['PATCH', '/api/clubs/1', 204, $adminHeader, json_encode($club)];
        yield ['PATCH', '/api/clubs/0', 404, $adminHeader, json_encode($club)];
        yield ['DELETE', '/api/clubs/3', 401];
        yield ['DELETE', '/api/clubs/3', 204, $adminHeader];
        yield ['DELETE', '/api/clubs/1', 500, $adminHeader];
        yield ['DELETE', '/api/clubs/0', 404, $adminHeader];
        yield ['PATCH', '/api/clubs/4/competitors/13', 401];
        yield ['PATCH', '/api/clubs/4/competitors/13', 204, $adminHeader];
        yield ['PATCH', '/api/clubs/0/competitors/13', 404, $adminHeader];
        yield ['PATCH', '/api/clubs/3/competitors/0', 404, $adminHeader];
        yield ['DELETE', '/api/clubs/1/competitors/1', 401];
        yield ['DELETE', '/api/clubs/1/competitors/1', 204, $adminHeader];
        yield ['DELETE', '/api/clubs/0/competitors/1', 404, $adminHeader];
        yield ['DELETE', '/api/clubs/1/competitors/0', 404, $adminHeader];
    }
}
