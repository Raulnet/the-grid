<?php

namespace App\Tests\Controller\Api;

class ApiLoungeControllerTest extends AbstractApiTestCase
{
    public function testLounges(): void
    {
        $this->truncateTableDataBase();
        $this->loadFixtures(['user.yml', 'competitor.yml', 'club.yml', 'championship1.yml']);
        $this->sendMultiRequest($this->getRequests());
    }

    private function getRequests(): iterable
    {
        $adminHeader = $this->getAuthHeader('admin');
        yield ['GET', '/api/stages/1/lounges', 401];
        yield ['GET', '/api/stages/1/lounges', 200, $adminHeader];
        yield ['GET', '/api/stages/0/lounges', 404, $adminHeader];

        yield ['GET', '/api/stages/1/lounges/1', 401];
        yield ['GET', '/api/stages/1/lounges/1', 200, $adminHeader];
        yield ['GET', '/api/stages/0/lounges/1', 404, $adminHeader];
        yield ['GET', '/api/stages/1/lounges/0', 404, $adminHeader];

        $lounge = json_encode([
            'title' => 'Lounge Title test',
            'description' => 'Lounge description test',
        ]);
        yield ['POST', '/api/stages/1/lounges', 401, [], $lounge];
        yield ['POST', '/api/stages/1/lounges', 201, $adminHeader, $lounge];
        yield ['POST', '/api/stages/0/lounges', 404, $adminHeader, $lounge];

        $lounge = json_encode([
            'title' => 'Lounge Title edited test',
            'description' => 'Lounge description edited test',
        ]);
        yield ['PATCH', '/api/stages/1/lounges/1', 401, [], $lounge];
        yield ['PATCH', '/api/stages/1/lounges/1', 204, $adminHeader, $lounge];
        yield ['PATCH', '/api/stages/0/lounges/1', 404, $adminHeader, $lounge];
        yield ['PATCH', '/api/stages/1/lounges/0', 404, $adminHeader, $lounge];

        yield ['DELETE', '/api/stages/1/lounges/1', 401];
        yield ['DELETE', '/api/stages/1/lounges/1', 204, $adminHeader];
        yield ['DELETE', '/api/stages/0/lounges/1', 404, $adminHeader];
        yield ['DELETE', '/api/stages/1/lounges/0', 404, $adminHeader];
    }
}
