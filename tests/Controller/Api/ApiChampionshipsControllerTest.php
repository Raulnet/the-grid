<?php

namespace App\Tests\Controller\Api;

class ApiChampionshipsControllerTest extends AbstractApiTestCase
{
    public function testChampionships(): void
    {
        $this->truncateTableDataBase();
        $this->loadFixtures(['user.yml', 'competitor.yml', 'club.yml', 'championship1.yml']);
        $this->sendMultiRequest($this->getRequests());
    }

    private function getRequests(): iterable
    {
        $adminHeader = $this->getAuthHeader('admin');
        yield ['GET', '/api/championships', 401];
        yield ['GET', '/api/championships', 200, $adminHeader];
        yield ['GET', '/api/championships/1', 401];
        yield ['GET', '/api/championships/1', 200, $adminHeader];
        yield ['GET', '/api/championships/0', 404, $adminHeader];

        $bodyContent = json_encode([
            'title' => 'championshipTest',
            'description' => 'test phpunit',
        ]);
        yield ['POST', '/api/championships', 401, [], $bodyContent];
        yield ['POST', '/api/championships', 201, $adminHeader, $bodyContent];

        $bodyContent = json_encode([
            'title' => 'championshipTest edited',
            'description' => 'test phpunit edited',
            'dateCreation' => '2099-01-01 00:00:01',
        ]);
        yield ['PATCH', '/api/championships/1', 204, $adminHeader, $bodyContent];
        yield ['PATCH', '/api/championships/1', 409, $adminHeader, '<wrong content />'];
        yield ['PATCH', '/api/championships/0', 404, $adminHeader, $bodyContent];

        yield ['PATCH', '/api/championships/1/championship-rule/3', 401];
        yield ['PATCH', '/api/championships/1/championship-rule/1', 204, $adminHeader];
        yield ['PATCH', '/api/championships/2/championship-rule/1', 404, $adminHeader];

        yield ['DELETE', '/api/championships/1/championship-rule/3', 401];
        yield ['DELETE', '/api/championships/1/championship-rule/1', 204, $adminHeader];
        yield ['DELETE', '/api/championships/2/championship-rule/1', 404, $adminHeader];
    }
}
