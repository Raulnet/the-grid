<?php

namespace App\Tests\Controller\Api;

class ChampionshipRulesControllerTest extends AbstractApiTestCase
{
    public function testChampionshipRules(): void
    {
        $this->truncateTableDataBase();
        $this->loadFixtures(['user.yml', 'competitor.yml', 'club.yml', 'championship1.yml']);
        $this->sendMultiRequest($this->getRequests());
    }

    private function getRequests(): iterable
    {
        $adminHeader = $this->getAuthHeader('admin');

        yield ['GET', '/api/championship-rules', 401];
        yield ['GET', '/api/championship-rules', 200, $adminHeader];
        yield ['GET', '/api/championship-rules/1', 401];
        yield ['GET', '/api/championship-rules/1', 200, $adminHeader];
        yield ['GET', '/api/championship-rules/0', 404, $adminHeader];

        $content = json_encode([
            'title' => 'test create stage',
            'description' => 'test description create stage',
            'rules' => 'Cuius acerbitati uxor grave accesserat incentivum, germanitate Augusti turgida supra modum.',
        ]);

        yield ['POST', '/api/championship-rules', 401, [], $content];
        yield ['POST', '/api/championship-rules', 201, $adminHeader, $content];

        $content = json_encode([
            'title' => 'test create stage',
            'description' => 'test description create stage',
            'rules' => 'Cuius acerbitati uxor grave accesserat incentivum, germanitate Augusti turgida supra modum.',
        ]);

        yield ['PATCH', '/api/championship-rules/1', 401, [], $content];
        yield ['PATCH', '/api/championship-rules/1', 204, $adminHeader, $content];
        yield ['PATCH', '/api/championship-rules/0', 404, $adminHeader, $content];

        yield ['DELETE', '/api/championship-rules/1', 401, [], $content];
        yield ['DELETE', '/api/championship-rules/1', 204, $adminHeader, $content];
        yield ['DELETE', '/api/championship-rules/0', 404, $adminHeader, $content];
    }
}
